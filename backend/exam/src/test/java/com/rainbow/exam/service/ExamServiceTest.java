package com.rainbow.exam.service;

import cn.hutool.core.util.IdUtil;
import com.rainbow.exam.MockMvcBaseTest;
import com.rainbow.exam.entity.*;
import com.rainbow.exam.repository.*;
import com.rainbow.exam.service.impl.ExamServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ExamServiceTest extends MockMvcBaseTest {
    @InjectMocks
    ExamServiceImpl examService;
    @Mock
    ExamRepository examRepository;
    @Mock
    QuestionRepository questionRepository;
    @Mock
    QuestionCategoryRepository questionCategoryRepository;
    @Mock
    QuestionOptionRepository questionOptionRepository;
    @Mock
    ExamRecordRepository examRecordRepository;

    @Test
    void should_return_record_when_judge() {
        String userId = "admin";
        String examId = "examId";
        HashMap<String, List<String>> answersMap = new HashMap<>();
        List<String> answer1 = new ArrayList<>();
        answer1.add("d89e5b9baf534245b227e72fa7fb70ee");
        List<String> answer2 = new ArrayList<>();
        answer2.add("ef4290f39e4c4e57b6e3a2323cddcfdb");

        answersMap.put("dc08433bb7604e90aa1ad0ea33845bde", answer1);
        answersMap.put("ef4290f39e4c4e57b6e3a2323cddcfdb", answer2);

        Exam exam = new Exam();
        exam.setExamId("1");
        exam.setExamName("TT评估");
        exam.setExamDescription("此评估适应所有团队～");
        exam.setExamScore(2);
        exam.setExamQuestionIds("dc08433bb7604e90aa1ad0ea33845bde" +
                "-ef4290f39e4c4e57b6e3a2323cddcfdb");
        exam.setExamQuestionIdsRadio("dc08433bb7604e90aa1ad0ea33845bde" +
                "-ef4290f39e4c4e57b6e3a2323cddcfdb");

        List<Question> questionList = new ArrayList<>();
        Question question1 = new Question();
        question1.setQuestionId("dc08433bb7604e90aa1ad0ea33845bde");
        question1.setQuestionName("需求管理");
        Question question2 = new Question();
        question1.setQuestionId("ef4290f39e4c4e57b6e3a2323cddcfdb");
        questionList.add(question1);
        questionList.add(question2);

        QuestionCategory questionCategory = new QuestionCategory();
        questionCategory.setQuestionCategoryName("沟通协作");

        List<QuestionOption> questionOptionList = new ArrayList<>();
        QuestionOption questionOption1 = new QuestionOption();
        questionOption1.setQuestionOptionId("d89e5b9baf534245b227e72fa7fb70ee");
        questionOption1.setQuestionOptionScore("2");
        QuestionOption questionOption2 = new QuestionOption();
        questionOption2.setQuestionOptionId("ef4290f39e4c4e57b6e3a2323cddcfdb");
        questionOption2.setQuestionOptionScore("1");
        questionOptionList.add(questionOption1);
        questionOptionList.add(questionOption2);

        when(examRepository.findById(any())).thenReturn(Optional.of(exam));
        when(questionRepository.findAllById(any())).thenReturn(questionList);
        when(questionCategoryRepository.findById(any())).thenReturn(Optional.of(questionCategory));
        when(questionOptionRepository.findAll()).thenReturn(questionOptionList);
        when(examRecordRepository.save(any())).thenReturn(any());
        ExamRecord examRecord = examService.judge(userId, examId, answersMap);
        assertThat(examRecord.getExamJoinScore() == 3.0f);

    }

    @Test
    void should_return_comments_when_evaluate() {
        String examId = "examID-1";
        String userId = "userID-1";
        // 构造传入的参数
        List<UserAnswer> listUserAnswer = new ArrayList<>();
        UserAnswer userAnswer1 = new UserAnswer();
        userAnswer1.setQuestionId("question-01");
        userAnswer1.setQuestionAnswers(Arrays.asList("answer_of_question-01"));
        userAnswer1.setComments("团队每天进行站会");

        UserAnswer userAnswer2 = new UserAnswer();
        userAnswer2.setQuestionId("question-02");
        userAnswer2.setQuestionAnswers(Arrays.asList("answer_of_question-02"));
        userAnswer2.setComments("团队具有持续集成流水线");

        listUserAnswer.add(userAnswer1);
        listUserAnswer.add(userAnswer2);

        ExamRecord examRecord = new ExamRecord();
        examRecord.setExamRecordId(IdUtil.simpleUUID());
        examRecord.setExamId(examId);

        examRecord.setExamJoinerId(userId);
        examRecord.setExamJoinDate(new Date());
        examRecord.setUserAnswers(listUserAnswer);
        examRecord.setExamTimeCost(0f);

        //构造问题选项
        QuestionOption questionOption1 = new QuestionOption();
        questionOption1.setQuestionOptionId("answer_of_question-01");
        questionOption1.setQuestionOptionScore("2");
        QuestionOption questionOption2 = new QuestionOption();
        questionOption2.setQuestionOptionId("answer_of_question-02");
        questionOption2.setQuestionOptionScore("1");
        when(examRecordRepository.save(any())).thenReturn(examRecord);
        when(questionOptionRepository.getOne(any())).thenReturn(questionOption1).thenReturn(questionOption2);


        ExamRecord examRecordResult = examService.evaluate("admin", "examId", listUserAnswer);
        assertThat(examRecordResult.getUserAnswers().get(0).getComments().equals("团队每天进行站会"));
        assertThat(examRecordResult.getExamTimeCost() == 0.5f);
    }

    @Test
    void should_return_correct_result_when_re_evaluate() {
        // 设置入参 listUserAnswer
        List<UserAnswer> listUserAnswer = new ArrayList<>();
        UserAnswer userAnswer1 = new UserAnswer();
        userAnswer1.setQuestionId("question-01");
        userAnswer1.setQuestionAnswers(Arrays.asList("answer_of_question-01"));
        userAnswer1.setComments("团队每天进行站会");
        UserAnswer userAnswer2 = new UserAnswer();
        userAnswer2.setQuestionId("question-02");
        userAnswer2.setQuestionAnswers(Arrays.asList("answer_of_question-02"));
        userAnswer2.setComments("团队具有持续集成流水线");
        listUserAnswer.add(userAnswer1);
        listUserAnswer.add(userAnswer2);

        //设置返回值
        ExamRecord examRecord = new ExamRecord();
        String examId = "examID-1";
        examRecord.setExamRecordId("recordId");
        examRecord.setExamId(examId);

        //构造问题选项
        QuestionOption questionOption1 = new QuestionOption();
        QuestionOption questionOption2 = new QuestionOption();
        questionOption1.setQuestionOptionId("answer_of_question-01");
        questionOption1.setQuestionOptionScore("2");
        questionOption2.setQuestionOptionId("answer_of_question-02");
        questionOption2.setQuestionOptionScore("1");

        when(examRecordRepository.getOne(any())).thenReturn(examRecord);
        when(examRecordRepository.save(any())).thenReturn(examRecord);
        when(questionOptionRepository.getOne(any())).thenReturn(questionOption1).thenReturn(questionOption2);

        ExamRecord examRecordResult = examService.reEvaluate("admin", "recordId", listUserAnswer);
        assertThat(examRecordResult.getUserAnswers().get(0).getComments().equals("团队每天进行站会"));
        assertThat(examRecordResult.getExamTimeCost() == 0.5f);
    }


}