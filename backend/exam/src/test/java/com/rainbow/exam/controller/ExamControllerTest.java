package com.rainbow.exam.controller;

import com.google.gson.Gson;
import com.rainbow.exam.MockMvcBaseTest;
import com.rainbow.exam.entity.ChartData;
import com.rainbow.exam.entity.ExamRecord;
import com.rainbow.exam.entity.UserAnswer;
import com.rainbow.exam.service.ExamService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ExamControllerTest extends MockMvcBaseTest {
    @InjectMocks
    ExamController examController;
    @Mock
    ExamService examService;

    @BeforeEach
    void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(examController).build();
    }

    // 新接口测试用例
    @Test
    void evaluateExam() throws Exception {
        // 构造传入的参数
        List<UserAnswer> listUserAnswer = new ArrayList<>();
        UserAnswer userAnswer1 = new UserAnswer();
        userAnswer1.setQuestionId("question-01");
        userAnswer1.setQuestionAnswers(Arrays.asList("answer_of_question-01"));
        userAnswer1.setComments("团队每天进行站会");

        UserAnswer userAnswer2 = new UserAnswer();
        userAnswer2.setQuestionId("question-02");
        userAnswer2.setQuestionAnswers(Arrays.asList("answer_of_question-02"));
        userAnswer2.setComments("团队具有持续集成流水线");

        listUserAnswer.add(userAnswer1);
        listUserAnswer.add(userAnswer2);

        //构造返回的结果
        ExamRecord examRecord = new ExamRecord();
        examRecord.setExamRecordId("1");
        examRecord.setExamId("1");
        examRecord.setAnswerOptionIds("$question-01@True_answer_of_question-01" +
                "$question-02@True_answer_of_question-02");
        examRecord.setExamJoinerId("user-01");
        examRecord.setExamJoinScore(5F);
        examRecord.setExamResultLevel((float) 1.33);
        examRecord.setExamJoinDate(new Date());
        examRecord.setExamTimeCost(50F);
        examRecord.setChartData(new ChartData());
        examRecord.setUserAnswers(listUserAnswer);

        when(examService.evaluate(any(), any(), any())).thenReturn(examRecord);

        this.mockMvc.perform(post("/exam/evaluate/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new Gson().toJson(listUserAnswer))
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("0"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.userAnswers[0].comments").value("团队每天进行站会"))
                .andReturn();
    }

    // 原接口测试用例
    @Test
    void finishExam() throws Exception {
        HashMap<String, List<String>> answersMap = new HashMap<>();
        List<String> answer1 = new ArrayList<>();
        answer1.add("d89e5b9baf534245b227e72fa7fb70ee");
        List<String> answer2 = new ArrayList<>();
        answer2.add("ef4290f39e4c4e57b6e3a2323cddcfdb");
        answersMap.put("question-01", answer1);
        answersMap.put("ef4290f39e4c4e57b6e3a2323cddcfdb", answer2);
//        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
//        params.add("user_id", "1");
        this.mockMvc.perform(post("/exam/finish/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("user_id", "1")
                        .content(new Gson().toJson(answersMap))
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("0"))
                .andReturn();
    }
}