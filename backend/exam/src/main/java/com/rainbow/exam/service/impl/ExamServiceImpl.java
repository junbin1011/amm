/***********************************************************
 * @Description : 评估服务接口实现
 * @author      : 梁山广(Laing Shan Guang)
 * @date        : 2019-05-28 08:06
 * @email       : liangshanguang2@gmail.com
 ***********************************************************/
package com.rainbow.exam.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.rainbow.exam.entity.*;
import com.rainbow.exam.repository.*;
import com.rainbow.exam.service.ExamService;
import com.rainbow.exam.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ExamServiceImpl implements ExamService {

    private final ExamRepository examRepository;

    private final ExamRecordRepository examRecordRepository;

    private final QuestionRepository questionRepository;

    private final UserRepository userRepository;

    private final QuestionLevelRepository questionLevelRepository;

    private final QuestionTypeRepository questionTypeRepository;

    private final QuestionCategoryRepository questionCategoryRepository;

    private final QuestionOptionRepository questionOptionRepository;

    public ExamServiceImpl(QuestionRepository questionRepository, UserRepository userRepository, QuestionLevelRepository questionLevelRepository, QuestionTypeRepository questionTypeRepository, QuestionCategoryRepository questionCategoryRepository, QuestionOptionRepository questionOptionRepository, ExamRepository examRepository, ExamRecordRepository examRecordRepository) {
        this.questionRepository = questionRepository;
        this.userRepository = userRepository;
        this.questionLevelRepository = questionLevelRepository;
        this.questionTypeRepository = questionTypeRepository;
        this.questionCategoryRepository = questionCategoryRepository;
        this.questionOptionRepository = questionOptionRepository;
        this.examRepository = examRepository;
        this.examRecordRepository = examRecordRepository;
    }

    @Override
    public QuestionPageVo getQuestionList(Integer pageNo, Integer pageSize) {
        // 按照日期降序排列
        Sort sort = new Sort(Sort.Direction.DESC, "updateTime");
        // 构造分页请求,注意前端面页面的分页是从1开始的，后端是从0开始地，所以要减去1哈
        PageRequest pageRequest = PageRequest.of(pageNo - 1, pageSize, sort);
        Page<Question> questionPage = questionRepository.findAll(pageRequest);
        QuestionPageVo questionPageVo = new QuestionPageVo();
        // 设置页码
        questionPageVo.setPageNo(pageNo);
        // 设置页大小
        questionPageVo.setPageSize(pageSize);
        // 设置总共有多少个元素
        questionPageVo.setTotalCount(questionPage.getTotalElements());
        // 设置一共有多少页
        questionPageVo.setTotalPage(questionPage.getTotalPages());
        // 当前页的问题列表
        List<Question> questionList = questionPage.getContent();
        // 需要自定义的question列表
        List<QuestionVo> questionVoList = new ArrayList<>();
        // 循环完成每个属性的定制
        for (Question question : questionList) {
            QuestionVo questionVo = new QuestionVo();
            // 先复制能复制的属性
            BeanUtils.copyProperties(question, questionVo);
            // 设置问题的创建者
            questionVo.setQuestionCreator(
                    Objects.requireNonNull(
                            userRepository.findById(
                                    question.getQuestionCreatorId()
                            ).orElse(null)
                    ).getUserUsername());


            // 设置题目的类别
            questionVo.setQuestionType(
                    Objects.requireNonNull(
                            questionTypeRepository.findById(
                                    question.getQuestionTypeId()
                            ).orElse(null)
                    ).getQuestionTypeDescription());

            // 设置题目分类，比如数学、语文、英语、生活、人文等
            questionVo.setQuestionCategory(
                    Objects.requireNonNull(
                            questionCategoryRepository.findById(
                                    question.getQuestionCategoryId()
                            ).orElse(null)
                    ).getQuestionCategoryName()
            );

            // 选项的自定义Vo列表
            List<QuestionOptionVo> optionVoList = new ArrayList<>();

            // 获得所有的选项列表
            List<QuestionOption> optionList = questionOptionRepository.findAllById(
                    Arrays.asList(question.getQuestionOptionIds().split("-"))
            );


            // 根据选项和答案的id相同设置optionVo的isAnswer属性
            for (QuestionOption option : optionList) {
                QuestionOptionVo optionVo = new QuestionOptionVo();
                BeanUtils.copyProperties(option, optionVo);
                optionVoList.add(optionVo);
            }

            // 设置题目的所有选项
            questionVo.setQuestionOptionVoList(optionVoList);

            questionVoList.add(questionVo);
        }
        questionPageVo.setQuestionVoList(questionVoList);
        return questionPageVo;
    }

    @Override
    public void updateQuestion(QuestionVo questionVo) {
        // 1.把需要的属性都设置好
        String questionName = questionVo.getQuestionName();
        StringBuilder questionAnswerOptionIds = new StringBuilder();
        List<QuestionOption> questionOptionList = new ArrayList<>();
        List<QuestionOptionVo> questionOptionVoList = questionVo.getQuestionOptionVoList();
        int size = questionOptionVoList.size();
        for (int i = 0; i < questionOptionVoList.size(); i++) {
            QuestionOptionVo questionOptionVo = questionOptionVoList.get(i);
            QuestionOption questionOption = new QuestionOption();
            BeanUtils.copyProperties(questionOptionVo, questionOption);
            questionOptionList.add(questionOption);
            if (questionOptionVo.getAnswer()) {
                if (i != size - 1) {
                    // 把更新后的答案的id加上去,记得用-连到一起
                    questionAnswerOptionIds.append(questionOptionVo.getQuestionOptionId()).append("-");
                } else {
                    // 最后一个不需要用-连接
                    questionAnswerOptionIds.append(questionOptionVo.getQuestionOptionId());
                }
            }

        }

        // 1.更新问题
        Question question = questionRepository.findById(questionVo.getQuestionId()).orElse(null);
        assert question != null;
        question.setQuestionName(questionName);
        question.setQuestionAnswerOptionIds(questionAnswerOptionIds.toString());
        questionRepository.save(question);

        // 2.更新所有的option
        questionOptionRepository.saveAll(questionOptionList);
    }

    @Override
    public void questionCreate(QuestionCreateVo questionCreateVo) {
        // 问题创建
        Question question = new Question();
        // 把能复制的属性都复制过来
        BeanUtils.copyProperties(questionCreateVo, question);
        // 设置下questionOptionIds和questionAnswerOptionIds，需要自己用Hutool生成下
        List<QuestionOption> questionOptionList = new ArrayList<>();
        List<QuestionOptionCreateVo> questionOptionCreateVoList = questionCreateVo.getQuestionOptionCreateVoList();
        for (QuestionOptionCreateVo questionOptionCreateVo : questionOptionCreateVoList) {
            QuestionOption questionOption = new QuestionOption();
            // 设置选项的的内容
            questionOption.setQuestionOptionContent(questionOptionCreateVo.getQuestionOptionContent());
            // 设置选项的id
            questionOption.setQuestionOptionId(IdUtil.simpleUUID());
            questionOptionList.add(questionOption);
        }
        // 把选项都存起来，然后才能用于下面设置Question的questionOptionIds和questionAnswerOptionIds
        questionOptionRepository.saveAll(questionOptionList);
        String questionOptionIds = "";
        String questionAnswerOptionIds = "";
        // 经过上面的saveAll方法，所有的option的主键id都已经持久化了
        for (int i = 0; i < questionOptionCreateVoList.size(); i++) {
            // 获取指定选项
            QuestionOptionCreateVo questionOptionCreateVo = questionOptionCreateVoList.get(i);
            // 获取保存后的指定对象
            QuestionOption questionOption = questionOptionList.get(i);
            questionOptionIds += questionOption.getQuestionOptionId() + "-";
            if (questionOptionCreateVo.getAnswer()) {
                // 如果是答案的话
                questionAnswerOptionIds += questionOption.getQuestionOptionId() + "-";
            }
        }

        questionOptionIds = replaceLastSeparator(questionOptionIds);
        // 设置选项id组成的字符串
        question.setQuestionOptionIds(questionOptionIds);

        // 自己生成问题的id
        question.setQuestionId(IdUtil.simpleUUID());
        // 保存问题到数据库
        questionRepository.save(question);
    }

    @Override
    public QuestionSelectionVo getSelections() {
        QuestionSelectionVo questionSelectionVo = new QuestionSelectionVo();
        questionSelectionVo.setQuestionCategoryList(questionCategoryRepository.findAll());
        questionSelectionVo.setQuestionLevelList(questionLevelRepository.findAll());
        questionSelectionVo.setQuestionTypeList(questionTypeRepository.findAll());

        return questionSelectionVo;
    }

    /**
     * 去除字符串最后的，防止split的时候出错
     *
     * @param str 原始字符串
     * @return
     */
    public static String trimMiddleLine(String str) {
        if (str.charAt(str.length() - 1) == '-') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    @Override
    public QuestionDetailVo getQuestionDetail(String id) {
        Question question = questionRepository.findById(id).orElse(null);
        QuestionDetailVo questionDetailVo = new QuestionDetailVo();
        questionDetailVo.setId(id);
        questionDetailVo.setName(question.getQuestionName());
        questionDetailVo.setDescription(question.getQuestionDescription());
        // 问题类型，单选题/多选题/判断题
        questionDetailVo.setType(
                Objects.requireNonNull(
                        questionTypeRepository.findById(
                                question.getQuestionTypeId()
                        ).orElse(null)
                ).getQuestionTypeDescription()
        );
        // 获取当前问题的选项
        String optionIdsStr = trimMiddleLine(question.getQuestionOptionIds());
        String[] optionIds = optionIdsStr.split("-");

        // 获取选项列表
        List<QuestionOption> optionList = new ArrayList<>();
        for (int i = 0; i < optionIds.length; i++) {
            questionOptionRepository.findById(optionIds[i]).ifPresent(optionList::add);
        }
        Collections.sort(optionList);
        questionDetailVo.setOptions(optionList);
        return questionDetailVo;
    }

    @Override
    public ExamPageVo getExamList(Integer pageNo, Integer pageSize) {
        // 获取评估列表
        // 按照日期降序排列
        Sort sort = new Sort(Sort.Direction.DESC, "updateTime");
        // 构造分页请求,注意前端面页面的分页是从1开始的，后端是从0开始地，所以要减去1哈
        PageRequest pageRequest = PageRequest.of(pageNo - 1, pageSize, sort);
        Page<Exam> examPage = examRepository.findAll(pageRequest);
        ExamPageVo examPageVo = new ExamPageVo();
        // 设置页码
        examPageVo.setPageNo(pageNo);
        // 设置每页有多少条数据
        examPageVo.setPageSize(pageSize);
        // 设置总共有多少个元素
        examPageVo.setTotalCount(examPage.getTotalElements());
        // 设置一共有多少页
        examPageVo.setTotalPage(examPage.getTotalPages());
        // 取出当前页的评估列表
        List<Exam> examList = examPage.getContent();
        // 需要自定义的exam列表
        List<ExamVo> examVoList = new ArrayList<>();
        // 循环完成每个属性的定制
        for (Exam exam : examList) {
            ExamVo examVo = new ExamVo();
            // 先尽量复制能复制的所有属性
            BeanUtils.copyProperties(exam, examVo);
            // 设置问题的创建者
            examVo.setExamCreator(
                    Objects.requireNonNull(
                            userRepository.findById(
                                    exam.getExamCreatorId()
                            ).orElse(null)
                    ).getUserUsername()
            );

            // 获取所有单选题列表，并赋值到ExamVo的属性ExamQuestionSelectVoRadioList上
            List<ExamQuestionSelectVo> radioQuestionVoList = new ArrayList<>();
            List<Question> radioQuestionList = questionRepository.findAllById(
                    Arrays.asList(exam.getExamQuestionIdsRadio().split("-"))
            );
            for (Question question : radioQuestionList) {
                ExamQuestionSelectVo radioQuestionVo = new ExamQuestionSelectVo();
                BeanUtils.copyProperties(question, radioQuestionVo);
                radioQuestionVoList.add(radioQuestionVo);
            }
            examVo.setExamQuestionSelectVoRadioList(radioQuestionVoList);

            // 把examVo加到examVoList中
            examVoList.add(examVo);
        }
        examPageVo.setExamVoList(examVoList);
        return examPageVo;
    }

    @Override
    public ExamQuestionTypeVo getExamQuestionType() {
        ExamQuestionTypeVo examQuestionTypeVo = new ExamQuestionTypeVo();
        // 获取所有单选题列表，并赋值到ExamVo的属性ExamQuestionSelectVoRadioList上
        List<ExamQuestionSelectVo> radioQuestionVoList = new ArrayList<>();
        List<Question> radioQuestionList = questionRepository.findAll();
        for (Question question : radioQuestionList) {
            ExamQuestionSelectVo radioQuestionVo = new ExamQuestionSelectVo();
            BeanUtils.copyProperties(question, radioQuestionVo);
            radioQuestionVoList.add(radioQuestionVo);
        }
        examQuestionTypeVo.setExamQuestionSelectVoRadioList(radioQuestionVoList);

        // 获取所有多选题列表，并赋值到ExamVo的属性ExamQuestionSelectVoCheckList上
//        List<ExamQuestionSelectVo> checkQuestionVoList = new ArrayList<>();
//        List<Question> checkQuestionList = questionRepository.findByQuestionTypeId(QuestionEnum.CHECK.getId());
//        for (Question question : checkQuestionList) {
//            ExamQuestionSelectVo checkQuestionVo = new ExamQuestionSelectVo();
//            BeanUtils.copyProperties(question, checkQuestionVo);
//            checkQuestionVoList.add(checkQuestionVo);
//        }
//        examQuestionTypeVo.setExamQuestionSelectVoCheckList(checkQuestionVoList);
//
//        // 获取所有多选题列表，并赋值到ExamVo的属性ExamQuestionSelectVoJudgeList上
//        List<ExamQuestionSelectVo> judgeQuestionVoList = new ArrayList<>();
//        List<Question> judgeQuestionList = questionRepository.findByQuestionTypeId(QuestionEnum.JUDGE.getId());
//        for (Question question : judgeQuestionList) {
//            ExamQuestionSelectVo judgeQuestionVo = new ExamQuestionSelectVo();
//            BeanUtils.copyProperties(question, judgeQuestionVo);
//            judgeQuestionVoList.add(judgeQuestionVo);
//        }
//        examQuestionTypeVo.setExamQuestionSelectVoJudgeList(judgeQuestionVoList);
        return examQuestionTypeVo;
    }

    @Override
    public Exam create(ExamCreateVo examCreateVo, String userId) {
        // 在线评估系统创建
        Exam exam = new Exam();
        BeanUtils.copyProperties(examCreateVo, exam);
        exam.setExamId(IdUtil.simpleUUID());
        exam.setExamCreatorId(userId);
        exam.setCreateTime(new Date());
        exam.setUpdateTime(new Date());
        // Todo:这两个日志后面是要在前端传入的，这里暂时定为当前日期
        exam.setExamStartDate(new Date());
        exam.setExamEndDate(new Date());
        String radioIdsStr = "";
        List<ExamQuestionSelectVo> radios = examCreateVo.getRadios();
        int radioCnt = 0;
        for (ExamQuestionSelectVo radio : radios) {
            if (radio.getChecked()) {
                radioIdsStr += radio.getQuestionId() + "-";
                radioCnt++;
            }
        }
        //如果没有选择题目，默认评估所有题目
        if (radioIdsStr.isEmpty()) {
            List<Question> questions = questionRepository.findAll();
            for (Question question : questions) {
                radioIdsStr += question.getQuestionId() + "-";
                radioCnt++;
            }
        }

        radioIdsStr = replaceLastSeparator(radioIdsStr);
        exam.setExamQuestionIds(radioIdsStr);
        // 设置各个题目的id
        exam.setExamQuestionIdsRadio(radioIdsStr);

        // 计算总分数
        int examScore = radioCnt;
        exam.setExamScore(examScore);
        examRepository.save(exam);
        return exam;
    }

    @Override
    public List<ExamCardVo> getExamCardList() {
        List<Exam> examList = examRepository.findAll();
        List<ExamCardVo> examCardVoList = new ArrayList<>();
        for (Exam exam : examList) {
            ExamCardVo examCardVo = new ExamCardVo();
            BeanUtils.copyProperties(exam, examCardVo);
            examCardVoList.add(examCardVo);
        }
        return examCardVoList;
    }

    @Override
    public ExamDetailVo getExamDetail(String id) {
        Exam exam = examRepository.findById(id).orElse(null);
        ExamDetailVo examDetailVo = new ExamDetailVo();
        examDetailVo.setExam(exam);
        assert exam != null;
        examDetailVo.setRadioIds(exam.getExamQuestionIdsRadio().split("-"));
        List<Question> questionList = questionRepository.findAllById(Arrays.asList(examDetailVo.getRadioIds()));
        HashMap<QuestionCategory, List<Question>> questionCategoryMap = new HashMap<>();
        for (Question question : questionList) {
            QuestionCategory questionCategory = questionCategoryRepository.findById(question.getQuestionCategoryId()).get();
            if (!questionCategoryMap.containsKey(questionCategory)) {
                List<Question> childQuestionList = new ArrayList<>();
                childQuestionList.add(question);
                questionCategoryMap.put(questionCategory, childQuestionList);
            } else {
                questionCategoryMap.get(questionCategory).add(question);
            }
        }

        //大维度进行排序
        List<QuestionCategory> sortQuestionCategoryList = new ArrayList<>(questionCategoryMap.keySet());
        Collections.sort(sortQuestionCategoryList);

        //得到结果
        HashMap<String, List<Question>> resultQuestionCategoryMap = new LinkedHashMap<>();
        for(QuestionCategory questionCategory :sortQuestionCategoryList){
            resultQuestionCategoryMap.put(questionCategory.getQuestionCategoryName(),questionCategoryMap.get(questionCategory));
        }
        examDetailVo.setQuestionCategoryMap(resultQuestionCategoryMap);
        return examDetailVo;
    }

    @Override
    public ExamRecord evaluate(String userId, String examId, List<UserAnswer> userAnswers) {
        ExamRecord examRecord = new ExamRecord();
        float totalScore = calculateTotalScore(userAnswers);
        float maturity = calculateMaturity(totalScore, userAnswers.size());
        String answerOptionIds = convertAnswerOptions(userAnswers);
        examRecord.setAnswerOptionIds(answerOptionIds);
        examRecord.setExamRecordId(IdUtil.simpleUUID());
        examRecord.setExamId(examId);
        examRecord.setExamJoinScore(totalScore);
        examRecord.setExamTimeCost(maturity);
        examRecord.setExamResultLevel(examRecord.getResultLevel());
        examRecord.setExamJoinerId(userId);
        examRecord.setExamJoinDate(new Date());
        examRecord.setUserAnswers(userAnswers);

        return examRecordRepository.save(examRecord);
    }

    @Override
    public ExamRecord reEvaluate(String userId, String recordId, List<UserAnswer> userAnswers) {
        ExamRecord examRecord = examRecordRepository.getOne(recordId);
        float totalScore = calculateTotalScore(userAnswers);
        float maturity = calculateMaturity(totalScore, userAnswers.size());
        String answerOptionIds = convertAnswerOptions(userAnswers);

        examRecord.setExamJoinScore(totalScore);
        examRecord.setExamTimeCost(maturity);
        examRecord.setAnswerOptionIds(answerOptionIds);
        examRecord.setExamResultLevel(examRecord.getResultLevel());
        examRecord.setExamJoinerId(userId);
        examRecord.setExamJoinDate(new Date());
        examRecord.setUserAnswers(userAnswers);

        return examRecordRepository.save(examRecord);
    }

    private String convertAnswerOptions(List<UserAnswer> userAnswers) {
        StringBuilder answerOptions = new StringBuilder();
        for (UserAnswer answer : userAnswers) {
            String answerOptionIds = listConcat(answer.getQuestionAnswers());
            answerOptions.append(answer.getQuestionId() + "@True_" + answerOptionIds + "$");
        }
        return replaceLastSeparator(answerOptions.toString());
    }

    private float calculateMaturity(float totalScore, int questionSize) {
        float basedOfLevel3 = totalScore / (questionSize * 3); //按照三级标准，每题3分为基准
        DecimalFormat decimalFormat= new DecimalFormat( ".00" );
        return Float.valueOf(decimalFormat.format(basedOfLevel3));
    }

    private float calculateTotalScore(List<UserAnswer> userAnswers) {
        float totalScore = 0;
        for (UserAnswer answer : userAnswers) {
            String questionOptionId = answer.getQuestionAnswers().get(0); // 当前只考虑了单选题的情况
            QuestionOption questionOption = questionOptionRepository.getOne(questionOptionId);
            totalScore += Float.valueOf(questionOption.getQuestionOptionScore());
        }
        return totalScore;
    }

    @Override
    public ExamRecord judge(String userId, String examId, HashMap<String, List<String>> answersMap) {
        // 4.计算得分，记录本次评估结果，存到ExamRecord中
        ExamRecord examRecord = new ExamRecord();
        examRecord.setExamRecordId(IdUtil.simpleUUID());
        examRecord.setExamId(examId);
        calculatorExamRecord(userId, answersMap, examRecord);
        return examRecord;
    }

    @Override
    public ExamRecord updateJudge(String userId, String recordId, HashMap<String, List<String>> answersMap) {

        ExamRecord examRecord = examRecordRepository.getOne(recordId);
        calculatorExamRecord(userId, answersMap, examRecord);
        return examRecord;
    }

    private void calculatorExamRecord(String userId, HashMap<String, List<String>> answersMap, ExamRecord examRecord) {
        // 开始评估判分啦~~~

        ExamDetailVo examDetailVo = getExamDetail(examRecord.getExamId());

        //获取问题列表
        List<String> questionIds = new ArrayList<>();

        List<String> radioIdList = Arrays.asList(examDetailVo.getRadioIds());
        questionIds.addAll(radioIdList);
        List<Question> questionList = questionRepository.findAllById(questionIds);
        Map<String, Question> questionMap = new HashMap<>();
        for (Question question : questionList) {
            questionMap.put(question.getQuestionId(), question);
        }
        // 根据正确答案和用户作答信息进行判分
        Set<String> questionIdsAnswer = answersMap.keySet();

        // 考生作答地每个题目的选项(题目和题目之间用$分隔，题目有多个选项地话用-分隔,题目和选项之间用_分隔),用于查看评估详情
        // 例子：题目1的id_作答选项1-作答选项2&题目2的id_作答选项1&题目3_作答选项1-作答选项2-作答选项3
        StringBuilder answerOptionIdsSb = new StringBuilder();
        // 用户此次评估的总分
        float totalScore = 0;
        List<QuestionOption> questionOptionList = questionOptionRepository.findAll();
        for (String questionId : questionIdsAnswer) {
            // 获取用户作答地这个题的答案信息
            Question question = questionMap.get(questionId);
            // 获取用户作答
            List<String> questionUserOptionIdList = answersMap.get(questionId);
            Collections.sort(questionUserOptionIdList);
            String userStr = listConcat(questionUserOptionIdList);
            String score = questionOptionList.stream()
                    .filter(questionOption -> questionOption.getQuestionOptionId().equals(userStr)).collect(Collectors.toList()).get(0).getQuestionOptionScore();
            // 累计本次评估得分
            totalScore += Integer.parseInt(score);
            // True代表题目答对
            answerOptionIdsSb.append(questionId + "@True_" + userStr + "$");


        }
        //分数转换为成熟度 分数/（问题数*3）
        int value = questionList.size() * 3;
        float cost = totalScore/value;
        BigDecimal b = new BigDecimal(cost);
        float result = b.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();


        // 注意去掉最后可能有的&_-
        examRecord.setAnswerOptionIds(replaceLastSeparator(answerOptionIdsSb.toString()));
        examRecord.setExamJoinerId(userId);
        examRecord.setExamJoinDate(new Date());

        examRecord.setExamJoinScore(totalScore);

        examRecord.setExamTimeCost(result);
        //等级换算
        examRecord.setExamResultLevel(examRecord.getResultLevel());
        examRecordRepository.save(examRecord);
    }


    @Override
    public List<ExamRecordVo> getExamRecordList(String userId) {
        // 获取指定用户下的评估记录列表
        List<ExamRecord> examRecordList = examRecordRepository.findByExamJoinerId(userId);
        List<ExamRecordVo> examRecordVoList = new ArrayList<>();
        List<QuestionOption> questionOptionList = questionOptionRepository.findAll();
        for (ExamRecord examRecord : examRecordList) {
            ExamRecordVo examRecordVo = new ExamRecordVo();
            Exam exam = examRepository.findById(examRecord.getExamId()).orElse(null);
            examRecordVo.setExam(exam);
            User user = userRepository.findById(userId).orElse(null);
            examRecordVo.setUser(user);
            //计算所有分数详情
            HashMap<String, String> scoreMap = new HashMap<>();
            scoreMap.put("", "");

            String answersStr = examRecord.getAnswerOptionIds();

            // 筛选统计每个分类下有哪些题目包含答案
            HashMap<Integer, List<Question>> questionCategoryMap = new HashMap<>();
            // $分隔题目,因为$在正则中有特殊用途(行尾)，所以需要括起来
            String[] questionArr = answersStr.split("[$]");

            List<Question> initQuestionList = questionRepository.findAllById(Arrays.asList(exam.getExamQuestionIds().split("-")));


            for (String questionStr : questionArr) {
                // 区分开题目标题和选项
                String[] questionTitleResultAndOption = questionStr.split("_");
                String[] questionTitleAndResult = questionTitleResultAndOption[0].split("@");
                String[] questionOptions = questionTitleResultAndOption[1].split("-");
                // 题目：答案选项
                String questionId = questionTitleAndResult[0];
                Question question = initQuestionList.stream()
                        .filter(question1 -> question1.getQuestionId().equals(questionId)).collect(Collectors.toList()).get(0);
                Integer questionCategoryId = question.getQuestionCategoryId();
                if (questionOptions.length > 0) {
                    question.setQuestionAnswerOptionIds(questionOptions[0]);
                }
                if (!questionCategoryMap.containsKey(questionCategoryId)) {
                    List<Question> questionList = new ArrayList<>();
                    questionList.add(question);
                    questionCategoryMap.put(questionCategoryId, questionList);
                } else {
                    questionCategoryMap.get(questionCategoryId).add(question);
                }
            }
            List<QuestionCategory> questionCategories = questionCategoryRepository.findAllById(questionCategoryMap.keySet());
            for (int id : questionCategoryMap.keySet()) {
                QuestionCategory questionCategory = questionCategories.stream()
                        .filter(questionCategory1 -> questionCategory1.getQuestionCategoryId().equals(id)).collect(Collectors.toList()).get(0);
                //计算该分类的所有得分
                List<Question> questionList = questionCategoryMap.get(id);
                float totalScore = 0;
                for (Question question : questionList) {
                    String score = questionOptionList.stream()
                            .filter(questionOption -> questionOption.getQuestionOptionId().equals(question.getQuestionAnswerOptionIds()))
                            .collect(Collectors.toList()).get(0).getQuestionOptionScore();
                    // 累计本次评估得分
                    totalScore += Integer.parseInt(score);
                }
                int value = questionList.size() * 3;
                float cost = totalScore/value;
                BigDecimal b = new BigDecimal(cost);
                float result = b.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
                scoreMap.put(questionCategory.getQuestionCategoryName(), exam.getResultLevel(result) + "");
            }
            examRecord.setScoreMap(scoreMap);
            ChartData chartData = new ChartData();
            chartData.setColumns(new ArrayList<>(scoreMap.keySet()));
            List<Map<String, String>> rows = new ArrayList<>();
            HashMap<String, String> fullScoreMap = new HashMap<>();
            fullScoreMap.put("", "");

            for (int id : questionCategoryMap.keySet()) {
                QuestionCategory questionCategory = questionCategories.stream()
                        .filter(questionCategory1 -> questionCategory1.getQuestionCategoryId().equals(id)).collect(Collectors.toList()).get(0);
                fullScoreMap.put(questionCategory.getQuestionCategoryName(), "5");
            }
            rows.add(fullScoreMap);
            rows.add(scoreMap);
            chartData.setRows(rows);
            examRecord.setChartData(chartData);
            examRecordVo.setExamRecord(examRecord);
            examRecordVoList.add(examRecordVo);
        }
        return examRecordVoList;
    }

    @Override
    public RecordDetailVo getRecordDetail(String recordId) {
        // 获取评估详情的封装对象
        ExamRecord record = examRecordRepository.findById(recordId).orElse(null);
        RecordDetailVo recordDetailVo = new RecordDetailVo();
        recordDetailVo.setExamRecord(record);
        // 用户的答案，需要解析
        HashMap<String, List<String>> answersMap = new HashMap<>();
        HashMap<String, String> resultsMap = new HashMap<>();
        HashMap<String, String> commentsMap = new HashMap<>();
        assert record != null;
        String answersStr = record.getAnswerOptionIds();
        // $分隔题目,因为$在正则中有特殊用途(行尾)，所以需要括起来
        String[] questionArr = answersStr.split("[$]");
        for (String questionStr : questionArr) {
            // 区分开题目标题和选项
            String[] questionTitleResultAndOption = questionStr.split("_");
            String[] questionTitleAndResult = questionTitleResultAndOption[0].split("@");
            String[] questionOptions = questionTitleResultAndOption[1].split("-");
            // 题目：答案选项
            answersMap.put(questionTitleAndResult[0], Arrays.asList(questionOptions));
            // 题目：True / False
            resultsMap.put(questionTitleAndResult[0], questionTitleAndResult[1]);
        }
        recordDetailVo.setAnswersMap(answersMap);
        recordDetailVo.setResultsMap(resultsMap);
        List<UserAnswer> userAnswers = record.getUserAnswers();
        for(UserAnswer userAnswer :userAnswers){
            commentsMap.put(userAnswer.getQuestionId(),userAnswer.getComments());
        }
        recordDetailVo.setCommentsMap(commentsMap);
        // 下面再计算正确答案的map
        ExamDetailVo examDetailVo = getExamDetail(record.getExamId());
        List<String> questionIdList = new ArrayList<>();
        questionIdList.addAll(Arrays.asList(examDetailVo.getRadioIds()));
        // 获取所有的问题对象
        HashMap<String, List<String>> answersRightMap = new HashMap<>();
        recordDetailVo.setAnswersRightMap(answersRightMap);
        return recordDetailVo;
    }

    /**
     * 把字符串最后一个字符-替换掉
     *
     * @param str 原始字符串
     * @return 替换掉最后一个-的字符串
     */
    private String replaceLastSeparator(String str) {
        String lastChar = str.substring(str.length() - 1);
        // 题目和题目之间用$分隔，题目有多个选项地话用-分隔,题目和选项之间用_分隔
        if ("-".equals(lastChar) || "_".equals(lastChar) || "$".equals(lastChar)) {
            str = StrUtil.sub(str, 0, str.length() - 1);
        }
        return str;
    }

    /**
     * 把字符串用-连接起来
     *
     * @param strList 字符串列表
     * @return 拼接好的字符串，记住要去掉最后面的-
     */
    private String listConcat(List<String> strList) {
        StringBuilder sb = new StringBuilder();
        for (String str : strList) {
            sb.append(str);
            sb.append("-");
        }
        return replaceLastSeparator(sb.toString());
    }
}
