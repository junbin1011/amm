package com.rainbow.exam.entity;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class ChartData {
    List<String> columns;
    List<Map<String, String>> rows;
}
