/***********************************************************
 * @Description : 评估接口
 * @author      : 梁山广(Laing Shan Guang)
 * @date        : 2019-05-28 08:05
 * @email       : liangshanguang2@gmail.com
 ***********************************************************/
package com.rainbow.exam.service;

import com.rainbow.exam.entity.Exam;
import com.rainbow.exam.entity.ExamRecord;
import com.rainbow.exam.entity.UserAnswer;
import com.rainbow.exam.vo.*;

import java.util.HashMap;
import java.util.List;

public interface ExamService {
    /**
     * 获取问题的列表
     *
     * @param pageNo   页码编号
     * @param pageSize 页面大小
     * @return 页面对象
     */
    QuestionPageVo getQuestionList(Integer pageNo, Integer pageSize);

    /**
     * 根据前端传过来的问题实体更新问题和选项
     *
     * @param questionVo 问题实体
     */
    void updateQuestion(QuestionVo questionVo);

    /**
     * 问题创建
     *
     * @param questionCreateVo 问题创建实体类
     */
    void questionCreate(QuestionCreateVo questionCreateVo);

    /**
     * 获取问题的选项、分类和难度的下拉列表
     *
     * @return 选项、分类和难度的封装对象
     */
    QuestionSelectionVo getSelections();

    /**
     * 获取问题详情
     *
     * @param id 问题的id
     * @return 问题详情的封装VO
     */
    QuestionDetailVo getQuestionDetail(String id);

    /**
     * 获取问题的列表
     *
     * @param pageNo   页码编号
     * @param pageSize 页面大小
     * @return 评估页面对象
     */
    ExamPageVo getExamList(Integer pageNo, Integer pageSize);

    /**
     * 获取所有问题的下拉列表，方便前端创建评估时筛选
     *
     * @return 适配前端的问题下拉列表
     */
    ExamQuestionTypeVo getExamQuestionType();

    /**
     * 根据前端组装的参数进行评估创建
     *
     * @param examCreateVo 前端组装的评估对象
     * @param userId       用户id
     * @return 创建好的评估
     */
    Exam create(ExamCreateVo examCreateVo, String userId);

    /**
     * 获取评估卡片列表
     *
     * @return 评估卡片列表
     */
    List<ExamCardVo> getExamCardList();

    /**
     * 根据评估的id获取评估的详情
     *
     * @param id exam表的主键
     * @return 评估详情的封装的VO对象
     */
    ExamDetailVo getExamDetail(String id);

    /**
     * 根据用户提交的作答信息进行判分
     *
     * @param userId     评估人
     * @param examId     参与的评估
     * @param answersMap 作答情况
     * @return 本次评估记录
     */
    ExamRecord judge(String userId, String examId, HashMap<String, List<String>> answersMap);

    /**
     * 根据用户提交的作答信息进行修改判分
     *
     * @param userId     评估人
     * @param recordId     参与的评估记录
     * @param answersMap 作答情况
     * @return 本次评估记录
     */
    ExamRecord updateJudge(String userId, String recordId, HashMap<String, List<String>> answersMap);

    /**
     * 根据用户id获取此用户的所有评估信息
     *
     * @param userId 用户id
     * @return 该用户的所有评估记录
     */
    List<ExamRecordVo> getExamRecordList(String userId);

    /**
     * 获取指定某次评估记录的详情
     *
     * @param recordId 评估记录的id
     * @return 评估详情
     */
    RecordDetailVo getRecordDetail(String recordId);

    /**
     *
     * @param userId 用户ID
     * @param examId 参与的评估
     * @param userAnswers 用户答案及comments
     * @return
     */
    ExamRecord evaluate(String userId, String examId, List<UserAnswer> userAnswers);

    /**
     *
     * @param userId 用户ID
     * @param recordId 评估记录的ID
     * @param userAnswers 用户答案及comments
     * @return
     */
    ExamRecord reEvaluate(String userId, String recordId, List<UserAnswer> userAnswers);
}
