/***********************************************************
 * @Description : 评估详情的实体类
 * @author      : 梁山广(Laing Shan Guang)
 * @date        : 2019-06-24 08:14
 * @email       : liangshanguang2@gmail.com
 ***********************************************************/
package com.rainbow.exam.vo;

import com.rainbow.exam.entity.Exam;
import com.rainbow.exam.entity.Question;
import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
public class ExamDetailVo {
    /**
     * 评估的基本信息对象
     */
    private Exam exam;
    /**
     * 单选题的列表
     */
    HashMap<String, List<Question>> questionCategoryMap;

    /**
     * 单选题的id数组
     */
    private String[] radioIds;

    /**
     * 多选题的id数组
     */
    private String[] checkIds;

    /**
     * 判断题的id数组
     */
    private String[] judgeIds;

}
