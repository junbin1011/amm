package com.rainbow.exam.entity;

import lombok.Data;

import java.util.List;

@Data
public class UserAnswer {
    private String questionId;
    private List<String> questionAnswers;
    private String comments;
}
