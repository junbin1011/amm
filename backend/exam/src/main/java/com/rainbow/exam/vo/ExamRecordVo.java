/***********************************************************
 * @Description : 评估记录VO
 * @author      : 梁山广(Liang Shan Guang)
 * @date        : 2019/10/25 7:42
 * @email       : liangshanguang2@gmail.com
 ***********************************************************/
package com.rainbow.exam.vo;

import com.rainbow.exam.entity.Exam;
import com.rainbow.exam.entity.ExamRecord;
import com.rainbow.exam.entity.User;
import lombok.Data;

@Data
public class ExamRecordVo {
    /**
     * 当前评估记录对应的评估
     */
    private Exam exam;

    /**
     * 当前评估对应的内容
     */
    private ExamRecord examRecord;

    /**
     * 参加评估的用户信息
     */
    private User user;
}
