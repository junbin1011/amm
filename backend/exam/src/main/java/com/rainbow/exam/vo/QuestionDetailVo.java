/***********************************************************
 * @Description : 问题详情的实体类
 * @author      : 梁山广(Laing Shan Guang)
 * @date        : 2019-10-20 09:51
 * @email       : liangshanguang2@gmail.com
 ***********************************************************/
package com.rainbow.exam.vo;

import com.rainbow.exam.entity.QuestionOption;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class QuestionDetailVo {
    /**
     * 问题的id
     */
    private String id;

    /**
     * 评估题目
     */
    private String name;

    /**
     * 评估描述
     */
    private String description;
    /**
     * 问题的类型
     */
    private String type;
    /**
     * 问题的选项
     */
    private List<QuestionOption> options;
    /**
     * 问题的答案,选项的id组成的数组
     */
    private List<String> answers = new ArrayList<>();
}
