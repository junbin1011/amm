/***********************************************************
 * @Description : 参加评估的记录，要有评估记录的id、参与者、参与时间、耗时、得分、得分级别(另建表)
 * @author      : 梁山广(Laing Shan Guang)
 * @date        : 2019/5/14 07:43
 * @email       : liangshanguang2@gmail.com
 ***********************************************************/
package com.rainbow.exam.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Data
@Entity
@TypeDef(name = "json", typeClass = JsonStringType.class)
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
public class ExamRecord {
    /**
     * 主键
     */
    @Id
    private String examRecordId;
    /**
     * 参与的评估的id
     */
    private String examId;

    /**
     * 考生作答地每个题目的选项(题目和题目之间用_分隔，题目有多个选项地话用-分隔),用于查看评估详情
     */
    private String answerOptionIds;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private List<UserAnswer> userAnswers;

    /**
     * 参与者，即user的id
     */
    private String examJoinerId;
    /**
     * 参加评估的日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date examJoinDate;
    /**
     * 评估耗时(秒)
     */
    private Float examTimeCost;
    /**
     * 评估得分
     */
    private Float examJoinScore;
    /**
     * 评估得分水平
     */
    private Float examResultLevel;

    public String getExamId() {
        return examId;
    }


    public float getResultLevel() {
        if (examTimeCost <= 0.19) {
            return 0;
        } else if (examTimeCost > 0.19 && examTimeCost <= 0.29) {
            return 0.5f;
        } else if (examTimeCost > 0.29 && examTimeCost <= 0.39) {
            return 1;
        } else if (examTimeCost > 0.39 && examTimeCost <= 0.49) {
            return 1.5f;
        } else if (examTimeCost > 0.49 && examTimeCost <= 0.59) {
            return 2;
        } else if (examTimeCost > 0.59 && examTimeCost <= 0.69) {
            return 2.5f;
        } else if (examTimeCost > 0.69 && examTimeCost <= 0.79) {
            return 3;
        } else if (examTimeCost > 0.79 && examTimeCost <= 0.89) {
            return 4;
        } else {
            return 5;
        }
    }


    /**
     * 获取子项目得分详情
     */
    @Transient
    private HashMap<String, String> scoreMap = new HashMap<>();

    /**
     * 获取子项目报表数据
     */
    @Transient
    private ChartData chartData = new ChartData();
}
