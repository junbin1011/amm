-- MySQL dump 10.13  Distrib 8.0.31, for macos12 (x86_64)
--
-- Host: 47.122.20.163    Database: exam
-- ------------------------------------------------------
-- Server version	8.0.32-0ubuntu0.22.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `action` (
  `action_id` int NOT NULL AUTO_INCREMENT COMMENT '前端页面操作表主键id',
  `action_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '前端操作的名字',
  `action_description` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '页面操作的描述',
  `default_check` tinyint(1) NOT NULL DEFAULT '0' COMMENT '当前操作是否需要校验,true为1,0为false',
  PRIMARY KEY (`action_id`) USING BTREE,
  UNIQUE KEY `action_name` (`action_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='前端操作比如增删改查等的权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action`
--

LOCK TABLES `action` WRITE;
/*!40000 ALTER TABLE `action` DISABLE KEYS */;
INSERT INTO `action` VALUES (1,'add','新增',0),(2,'query','查询',0),(3,'get','详情',0),(4,'update','修改',0),(5,'delete','删除',0),(6,'import','导入',0),(7,'export','导出',0);
/*!40000 ALTER TABLE `action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam`
--

DROP TABLE IF EXISTS `exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam` (
  `exam_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '考试表的主键',
  `exam_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '考试名称',
  `exam_avatar` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '考试的预览图',
  `exam_description` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '考试描述',
  `exam_question_ids` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci COMMENT '当前考试下的题目的id用-连在一起地字符串',
  `exam_question_ids_radio` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci COMMENT '当前考试下的题目单选题的id用-连在一起地字符串',
  `exam_question_ids_check` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci COMMENT '当前考试下的题目多选题的id用-连在一起地字符串',
  `exam_question_ids_judge` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci COMMENT '当前考试下的题目判断题的id用-连在一起地字符串',
  `exam_score` int NOT NULL DEFAULT '0' COMMENT '当前考试的总分数',
  `exam_score_radio` int DEFAULT '0' COMMENT '当前考试每个单选题的分数',
  `exam_score_check` int DEFAULT '0' COMMENT '当前考试每个多选题的分数',
  `exam_score_judge` int DEFAULT '0' COMMENT '当前考试每个判断题的分数',
  `exam_creator_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '考试创建者的用户id',
  `exam_time_limit` int DEFAULT '0' COMMENT '考试的时间限制，单位为分钟',
  `exam_start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '考试有效期开始时间',
  `exam_end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '考试有效期结束时间',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`exam_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='考试的详细信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam`
--

LOCK TABLES `exam` WRITE;
/*!40000 ALTER TABLE `exam` DISABLE KEYS */;
INSERT INTO `exam` VALUES ('174ce56964f1481b9e52e04866a76c25','彩虹风暴敏捷成熟度评估',NULL,'教练：梁珊老师、党小鹏老师','015a392d233d40e5b80223dd7dd49787-178e630d2c944c03a92da3ba536d54a0-39b3a29b438c4ab1a5d2e072c4fa97cb-727a3de6c78d48c0b8fc9f336342871e-b1c4b6393e0d46338180a4a1017456f5-e20f61aaba3b4e7caeb365286954c4ce','015a392d233d40e5b80223dd7dd49787-178e630d2c944c03a92da3ba536d54a0-39b3a29b438c4ab1a5d2e072c4fa97cb-727a3de6c78d48c0b8fc9f336342871e-b1c4b6393e0d46338180a4a1017456f5-e20f61aaba3b4e7caeb365286954c4ce',NULL,NULL,6,NULL,NULL,NULL,'a1b661031adf4a8f969f1869d479fe74',NULL,'2020-08-25 04:26:45','2020-08-25 04:26:45','2020-08-25 04:26:45','2020-08-25 04:26:45'),('83bf7a3164a345c1b0aeb60fc8c5de71','Test AMM',NULL,'amm','015a392d233d40e5b80223dd7dd49787-178e630d2c944c03a92da3ba536d54a0-b1c4b6393e0d46338180a4a1017456f5','015a392d233d40e5b80223dd7dd49787-178e630d2c944c03a92da3ba536d54a0-b1c4b6393e0d46338180a4a1017456f5',NULL,NULL,3,NULL,NULL,NULL,'a1b661031adf4a8f969f1869d479fe74',NULL,'2023-01-15 03:20:37','2023-01-15 03:20:37','2023-01-15 03:20:37','2023-01-15 03:20:37'),('cf46ea1e2dc14c4589c6e141286a5043','VIVO成熟度评估',NULL,'VIVO成熟度评估','015a392d233d40e5b80223dd7dd49787-08f47737a972452baa0af400944f768f-0ef428d1eed4404397d5f536282bec70-104d70807d05482ea438d0b60bdf478e-16934f014080482fa165eaf6ef0526d9-178e630d2c944c03a92da3ba536d54a0-1bf6e8ecee59498789c755852c78b05d-25dbd9aeb47a457e990cf85dd1fa15f0-2aa0277647dd4ea4ab0c685fd1768a82-31c56e786c1740519606114b5d599acb-31cfed31d01b4a018a0e18a202de5c1e-39b3a29b438c4ab1a5d2e072c4fa97cb-3f6db79de9114ebfa8e11028abcfe72a-44f9fa04b17b45fc9e6dc9d49ac514b3-4d7e15ef0ea24a97b3d2880e7bd33024-4e16da26b0ce4f46afdb6674030793f7-501961e1a05c4902912418649d27f92f-528f066bd9f7404b99ef45d93e00d5a4-56039b89457346848d0d3efb4357ae6d-5d8df14abcdf4282932e07161b97c815-5f143be5b8ac47a7a84957ef1eff9554-5f41005125e74208bfe3e81819cfb630-68b8215b93124b79b73ffe3c039b6631-727a3de6c78d48c0b8fc9f336342871e-72e0bb2574d649f18716b266d542456e-77dd4952523941baaa85dca62c9fc0ac-79d2811c4ec04211bd9eceb6ea21c347-7b2f66bd6f9e45ac9ecc629638da1cb8-7f591c4e8bb34d06ac3eb37ae506379d-8231ec6503a4485b8cb4828d07bf76ea-875fe33b87444f0086e893cf9aa2ec16-88247243fedb4241b93ce565a4db1d6d-8d7b5ca75a5748d6bb93fb150993a5b3-920e56d5cdeb4813ad9b2ddf7154a27f-935dbb6e4919464da0537ace35b27d66-941a7702e89f460c9f0746e257ac5613-9a88883749f34d8dafcd0d163f48e1bb-9df47949d02e410091d2705053cde6ed-9f0389beeea044098061ea256ffaeba1-a37c3526f1ae49a4a3a77c16c7e32607-a5df7c06a7e942d189a68b325613d5db-a5e81890180d4be0ae8712b9bc6ed41c-abefb1996d4844dfac020237bf69991d-ac8afe6ff57c4246947f396da6bec63c-ad7e05a6f421448092e43a411f5c087e-b1c4b6393e0d46338180a4a1017456f5-bf102c56d9a6407289ba1cd9b11b4ccb-bff114d05ccd40a1937db177f7262e95-c117c70ecdd14970b31c544ed36a6fcc-c348c930dba64c01be1e54ab080b1f2d-c3a9f0c044264f73a4436a7ed91941d2-c65afaa22d5243db9c20230953fd5cc4-cb2d4a08df4a430a83c1c48a1366385c-d1f631a3e83e41678f8c8e2fec4a4c93-d1f8fef52d8f46308414cda76a766ed8-d2e8339aa8e04e36afd253a5be3b0d72-dc08433bb7604e90aa1ad0ea33845bde-e20f61aaba3b4e7caeb365286954c4ce-e5e93172384c47f48e434160bed181fe-ef4290f39e4c4e57b6e3a2323cddcfdb-f4a724372efc41f4b76efc4cb610ae3e-fc048e5dbc7b45a2a76a0b36dc253549-fc4cfd48c128469dbdf8a7620856ff29-fd23eb882224422595356366b8de7198','015a392d233d40e5b80223dd7dd49787-08f47737a972452baa0af400944f768f-0ef428d1eed4404397d5f536282bec70-104d70807d05482ea438d0b60bdf478e-16934f014080482fa165eaf6ef0526d9-178e630d2c944c03a92da3ba536d54a0-1bf6e8ecee59498789c755852c78b05d-25dbd9aeb47a457e990cf85dd1fa15f0-2aa0277647dd4ea4ab0c685fd1768a82-31c56e786c1740519606114b5d599acb-31cfed31d01b4a018a0e18a202de5c1e-39b3a29b438c4ab1a5d2e072c4fa97cb-3f6db79de9114ebfa8e11028abcfe72a-44f9fa04b17b45fc9e6dc9d49ac514b3-4d7e15ef0ea24a97b3d2880e7bd33024-4e16da26b0ce4f46afdb6674030793f7-501961e1a05c4902912418649d27f92f-528f066bd9f7404b99ef45d93e00d5a4-56039b89457346848d0d3efb4357ae6d-5d8df14abcdf4282932e07161b97c815-5f143be5b8ac47a7a84957ef1eff9554-5f41005125e74208bfe3e81819cfb630-68b8215b93124b79b73ffe3c039b6631-727a3de6c78d48c0b8fc9f336342871e-72e0bb2574d649f18716b266d542456e-77dd4952523941baaa85dca62c9fc0ac-79d2811c4ec04211bd9eceb6ea21c347-7b2f66bd6f9e45ac9ecc629638da1cb8-7f591c4e8bb34d06ac3eb37ae506379d-8231ec6503a4485b8cb4828d07bf76ea-875fe33b87444f0086e893cf9aa2ec16-88247243fedb4241b93ce565a4db1d6d-8d7b5ca75a5748d6bb93fb150993a5b3-920e56d5cdeb4813ad9b2ddf7154a27f-935dbb6e4919464da0537ace35b27d66-941a7702e89f460c9f0746e257ac5613-9a88883749f34d8dafcd0d163f48e1bb-9df47949d02e410091d2705053cde6ed-9f0389beeea044098061ea256ffaeba1-a37c3526f1ae49a4a3a77c16c7e32607-a5df7c06a7e942d189a68b325613d5db-a5e81890180d4be0ae8712b9bc6ed41c-abefb1996d4844dfac020237bf69991d-ac8afe6ff57c4246947f396da6bec63c-ad7e05a6f421448092e43a411f5c087e-b1c4b6393e0d46338180a4a1017456f5-bf102c56d9a6407289ba1cd9b11b4ccb-bff114d05ccd40a1937db177f7262e95-c117c70ecdd14970b31c544ed36a6fcc-c348c930dba64c01be1e54ab080b1f2d-c3a9f0c044264f73a4436a7ed91941d2-c65afaa22d5243db9c20230953fd5cc4-cb2d4a08df4a430a83c1c48a1366385c-d1f631a3e83e41678f8c8e2fec4a4c93-d1f8fef52d8f46308414cda76a766ed8-d2e8339aa8e04e36afd253a5be3b0d72-dc08433bb7604e90aa1ad0ea33845bde-e20f61aaba3b4e7caeb365286954c4ce-e5e93172384c47f48e434160bed181fe-ef4290f39e4c4e57b6e3a2323cddcfdb-f4a724372efc41f4b76efc4cb610ae3e-fc048e5dbc7b45a2a76a0b36dc253549-fc4cfd48c128469dbdf8a7620856ff29-fd23eb882224422595356366b8de7198',NULL,NULL,64,NULL,NULL,NULL,'a1b661031adf4a8f969f1869d479fe74',NULL,'2023-01-17 00:37:58','2023-01-17 00:37:58','2023-01-17 00:37:58','2023-01-17 00:37:58');
/*!40000 ALTER TABLE `exam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_record`
--

DROP TABLE IF EXISTS `exam_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_record` (
  `exam_record_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '考试记录表的主键',
  `exam_joiner_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '考试参与者的用户id',
  `exam_join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '参加考试的时间',
  `exam_time_cost` float DEFAULT '0' COMMENT '完成考试所用的时间,单位分钟',
  `exam_join_score` float NOT NULL DEFAULT '0' COMMENT '参与考试的实际得分',
  `exam_result_level` float DEFAULT '0' COMMENT '考试结果的等级',
  `answer_option_ids` mediumtext CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `exam_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`exam_record_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='考试记录表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_record`
--

LOCK TABLES `exam_record` WRITE;
/*!40000 ALTER TABLE `exam_record` DISABLE KEYS */;
INSERT INTO `exam_record` VALUES ('02050470e5b947e0bd72e1444ee91f7e','a1b661031adf4a8f969f1869d479fe74','2023-01-20 12:26:45',0.72,13,3,'178e630d2c944c03a92da3ba536d54a0@True_5277b1327e5f496eb2ef74eb3cfad690$b1c4b6393e0d46338180a4a1017456f5@True_ed3b5b8a06544ec5b7d81b62879719e1$727a3de6c78d48c0b8fc9f336342871e@True_d5232efb29da4790bc0189267557bad2$015a392d233d40e5b80223dd7dd49787@True_9d24347e52374a26938c749f5abbe021$39b3a29b438c4ab1a5d2e072c4fa97cb@True_d780694c60c341a495bc9ed686644464$e20f61aaba3b4e7caeb365286954c4ce@True_ca0bacf113044a87bf0f6027112967c2','174ce56964f1481b9e52e04866a76c25'),('3c712535ba264c2798c8022588989fab','a1b661031adf4a8f969f1869d479fe74','2023-01-15 12:17:52',0.61,11,2.5,'178e630d2c944c03a92da3ba536d54a0@True_eb278bf1403a4d96b6c958a2d8dbf154$b1c4b6393e0d46338180a4a1017456f5@True_ed3b5b8a06544ec5b7d81b62879719e1$727a3de6c78d48c0b8fc9f336342871e@True_d5232efb29da4790bc0189267557bad2$015a392d233d40e5b80223dd7dd49787@True_dbba3b5c93b74f9986e5278e3f53910c$39b3a29b438c4ab1a5d2e072c4fa97cb@True_d780694c60c341a495bc9ed686644464$e20f61aaba3b4e7caeb365286954c4ce@True_63e23813ca204e97ae3beade42299f30','174ce56964f1481b9e52e04866a76c25'),('46f01f595f4d428b9727d09da4f23542','a1b661031adf4a8f969f1869d479fe74','2020-08-25 08:23:33',0.28,5,0.5,'178e630d2c944c03a92da3ba536d54a0@True_50fcf1a6bb1d49de91194be946a3398c$b1c4b6393e0d46338180a4a1017456f5@True_ed3b5b8a06544ec5b7d81b62879719e1$727a3de6c78d48c0b8fc9f336342871e@True_e28ced22550e484eac308c3253057b55$015a392d233d40e5b80223dd7dd49787@True_d89e5b9baf534245b227e72fa7fb70ee$39b3a29b438c4ab1a5d2e072c4fa97cb@True_ab1990055ddf4980a9288c350096da6d$e20f61aaba3b4e7caeb365286954c4ce@True_ca0bacf113044a87bf0f6027112967c2','174ce56964f1481b9e52e04866a76c25'),('60b3e552237c4f4fafb54a416be928e8','a1b661031adf4a8f969f1869d479fe74','2023-02-02 20:48:19',0.33,3,1,'178e630d2c944c03a92da3ba536d54a0@True_50fcf1a6bb1d49de91194be946a3398c$b1c4b6393e0d46338180a4a1017456f5@True_5e5bc2dbb84743b1ba615ef22245f4f6$015a392d233d40e5b80223dd7dd49787@True_d89e5b9baf534245b227e72fa7fb70ee','83bf7a3164a345c1b0aeb60fc8c5de71'),('63561e36f99643268d9c0ffadbda0e55','a1b661031adf4a8f969f1869d479fe74','2023-01-20 09:08:29',0.33,3,1,'178e630d2c944c03a92da3ba536d54a0@True_50fcf1a6bb1d49de91194be946a3398c$b1c4b6393e0d46338180a4a1017456f5@True_db89b05b80d543eb8fb0870bb5650948$015a392d233d40e5b80223dd7dd49787@True_29277d4c4fd34981b68c8fcec493e44b','83bf7a3164a345c1b0aeb60fc8c5de71'),('69afae6296d4448fbc15713a0234373e','79392778a90d4639a297dbd0bae0f779','2023-01-17 07:31:29',0.67,6,2.5,'178e630d2c944c03a92da3ba536d54a0@True_eb278bf1403a4d96b6c958a2d8dbf154$b1c4b6393e0d46338180a4a1017456f5@True_ed3b5b8a06544ec5b7d81b62879719e1$015a392d233d40e5b80223dd7dd49787@True_dbba3b5c93b74f9986e5278e3f53910c','83bf7a3164a345c1b0aeb60fc8c5de71'),('701a9b508eeb4e72b689c0543ee2b6d6','68042014e23c4ebea7234cb9c77cee5c','2023-01-17 11:49:05',0.07,14,0,'178e630d2c944c03a92da3ba536d54a0@True_eb278bf1403a4d96b6c958a2d8dbf154$16934f014080482fa165eaf6ef0526d9@True_9d24347e52374a26938c749f5abbe021$104d70807d05482ea438d0b60bdf478e@True_29277d4c4fd34981b68c8fcec493e44b$1bf6e8ecee59498789c755852c78b05d@True_9d24347e52374a26938c749f5abbe021$0ef428d1eed4404397d5f536282bec70@True_9d24347e52374a26938c749f5abbe021$015a392d233d40e5b80223dd7dd49787@True_d89e5b9baf534245b227e72fa7fb70ee$08f47737a972452baa0af400944f768f@True_dbba3b5c93b74f9986e5278e3f53910c','cf46ea1e2dc14c4589c6e141286a5043'),('97ae9434f638439db9f75f5f9184108d','a1b661031adf4a8f969f1869d479fe74','2023-01-20 09:08:10',0.67,6,2.5,'178e630d2c944c03a92da3ba536d54a0@True_eb278bf1403a4d96b6c958a2d8dbf154$b1c4b6393e0d46338180a4a1017456f5@True_ed3b5b8a06544ec5b7d81b62879719e1$015a392d233d40e5b80223dd7dd49787@True_dbba3b5c93b74f9986e5278e3f53910c','83bf7a3164a345c1b0aeb60fc8c5de71'),('b1e89752fead4077af9e5624f475dddc','a1b661031adf4a8f969f1869d479fe74','2023-02-03 14:59:37',0.02,4,0,'1bf6e8ecee59498789c755852c78b05d@True_d89e5b9baf534245b227e72fa7fb70ee$015a392d233d40e5b80223dd7dd49787@True_29277d4c4fd34981b68c8fcec493e44b$08f47737a972452baa0af400944f768f@True_d89e5b9baf534245b227e72fa7fb70ee$39b3a29b438c4ab1a5d2e072c4fa97cb@True_d780694c60c341a495bc9ed686644464','cf46ea1e2dc14c4589c6e141286a5043'),('ee2212afff094e68a82c1536f58c4dcd','a1b661031adf4a8f969f1869d479fe74','2023-02-03 15:05:40',0.69,133,2.5,'f4a724372efc41f4b76efc4cb610ae3e@True_29277d4c4fd34981b68c8fcec493e44b$3f6db79de9114ebfa8e11028abcfe72a@True_d54102b4c22c4eff9ecd9a532478777d$fd23eb882224422595356366b8de7198@True_29277d4c4fd34981b68c8fcec493e44b$c348c930dba64c01be1e54ab080b1f2d@True_98ef90d88b7b4e9285c26fdfa9762f54$abefb1996d4844dfac020237bf69991d@True_29277d4c4fd34981b68c8fcec493e44b$d1f8fef52d8f46308414cda76a766ed8@True_29277d4c4fd34981b68c8fcec493e44b$7f591c4e8bb34d06ac3eb37ae506379d@True_4753a830a38d4b8d8b4d8506950bf3d1$79d2811c4ec04211bd9eceb6ea21c347@True_29277d4c4fd34981b68c8fcec493e44b$4e16da26b0ce4f46afdb6674030793f7@True_c987c8afa5a14baab968d3ae6b63e390$88247243fedb4241b93ce565a4db1d6d@True_dde75d5c3d8741aabe971235e044f81e$16934f014080482fa165eaf6ef0526d9@True_29277d4c4fd34981b68c8fcec493e44b$1bf6e8ecee59498789c755852c78b05d@True_29277d4c4fd34981b68c8fcec493e44b$d1f631a3e83e41678f8c8e2fec4a4c93@True_d7e17c36292e464486cb90e01ca23d4d$5f41005125e74208bfe3e81819cfb630@True_29277d4c4fd34981b68c8fcec493e44b$e20f61aaba3b4e7caeb365286954c4ce@True_716f8c4995a54f8395d1a9842e36f8fe$fc4cfd48c128469dbdf8a7620856ff29@True_8f0ac1927722494483fdc77e39ea163f$a5e81890180d4be0ae8712b9bc6ed41c@True_29277d4c4fd34981b68c8fcec493e44b$c65afaa22d5243db9c20230953fd5cc4@True_49db849af3554873a36786498b12fd08$e5e93172384c47f48e434160bed181fe@True_e3c23e899eb94c00979d04a77e4a4100$727a3de6c78d48c0b8fc9f336342871e@True_d5232efb29da4790bc0189267557bad2$08f47737a972452baa0af400944f768f@True_29277d4c4fd34981b68c8fcec493e44b$4d7e15ef0ea24a97b3d2880e7bd33024@True_29277d4c4fd34981b68c8fcec493e44b$ac8afe6ff57c4246947f396da6bec63c@True_bc9a0957c0ae451c984dee4e1ea4b74a$104d70807d05482ea438d0b60bdf478e@True_29277d4c4fd34981b68c8fcec493e44b$25dbd9aeb47a457e990cf85dd1fa15f0@True_29277d4c4fd34981b68c8fcec493e44b$bf102c56d9a6407289ba1cd9b11b4ccb@True_805e732a65ba4bedaf31925ec5a4e928$7b2f66bd6f9e45ac9ecc629638da1cb8@True_707f5f38348144eba6af2dc40100e18f$8231ec6503a4485b8cb4828d07bf76ea@True_de106849ca2641b3927542ac81392d6b$178e630d2c944c03a92da3ba536d54a0@True_5277b1327e5f496eb2ef74eb3cfad690$ad7e05a6f421448092e43a411f5c087e@True_29277d4c4fd34981b68c8fcec493e44b$a5df7c06a7e942d189a68b325613d5db@True_5c91884aae70446b93e5c8a9ef884ad6$77dd4952523941baaa85dca62c9fc0ac@True_29277d4c4fd34981b68c8fcec493e44b$ef4290f39e4c4e57b6e3a2323cddcfdb@True_29277d4c4fd34981b68c8fcec493e44b$9f0389beeea044098061ea256ffaeba1@True_29277d4c4fd34981b68c8fcec493e44b$d2e8339aa8e04e36afd253a5be3b0d72@True_227d18f26e5147d08153b666b6061036$501961e1a05c4902912418649d27f92f@True_29277d4c4fd34981b68c8fcec493e44b$875fe33b87444f0086e893cf9aa2ec16@True_29277d4c4fd34981b68c8fcec493e44b$5d8df14abcdf4282932e07161b97c815@True_29277d4c4fd34981b68c8fcec493e44b$8d7b5ca75a5748d6bb93fb150993a5b3@True_2f845e0c2d7a4c018fb2aff73ccfe27b$9df47949d02e410091d2705053cde6ed@True_29277d4c4fd34981b68c8fcec493e44b$015a392d233d40e5b80223dd7dd49787@True_29277d4c4fd34981b68c8fcec493e44b$39b3a29b438c4ab1a5d2e072c4fa97cb@True_e05134eff2124345a58004ca501988c3$2aa0277647dd4ea4ab0c685fd1768a82@True_018d22b3d0cb4e67a9df8d23bdcb6ba9$fc048e5dbc7b45a2a76a0b36dc253549@True_29277d4c4fd34981b68c8fcec493e44b$c117c70ecdd14970b31c544ed36a6fcc@True_bc9a0957c0ae451c984dee4e1ea4b74a$dc08433bb7604e90aa1ad0ea33845bde@True_29277d4c4fd34981b68c8fcec493e44b$935dbb6e4919464da0537ace35b27d66@True_bcba47f4c69d434eabc1e2b3521ac07d$31c56e786c1740519606114b5d599acb@True_45f7f6e27def4944ac141cc26eaebb74$bff114d05ccd40a1937db177f7262e95@True_29277d4c4fd34981b68c8fcec493e44b$0ef428d1eed4404397d5f536282bec70@True_29277d4c4fd34981b68c8fcec493e44b$31cfed31d01b4a018a0e18a202de5c1e@True_29277d4c4fd34981b68c8fcec493e44b$920e56d5cdeb4813ad9b2ddf7154a27f@True_29277d4c4fd34981b68c8fcec493e44b$9a88883749f34d8dafcd0d163f48e1bb@True_0c7265efc30644479492b9afa4c97007$cb2d4a08df4a430a83c1c48a1366385c@True_d7a116d9d8e443a297953beb33497745','cf46ea1e2dc14c4589c6e141286a5043');
/*!40000 ALTER TABLE `exam_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_record_level`
--

DROP TABLE IF EXISTS `exam_record_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `exam_record_level` (
  `exam_record_level_id` int NOT NULL AUTO_INCREMENT COMMENT '考试结果等级表的主键',
  `exam_record_level_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '考试结果等级的名称',
  `exam_record_level_description` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '考试结果等级的详细阐述',
  PRIMARY KEY (`exam_record_level_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='考试结果的等级';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_record_level`
--

LOCK TABLES `exam_record_level` WRITE;
/*!40000 ALTER TABLE `exam_record_level` DISABLE KEYS */;
INSERT INTO `exam_record_level` VALUES (1,'excellent','优秀'),(2,'good','良好'),(3,'normal','一般'),(4,'pass','及格'),(5,'fail','不及格');
/*!40000 ALTER TABLE `exam_record_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=FIXED;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1),(1),(1),(1),(1),(1),(1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `page` (
  `page_id` int NOT NULL AUTO_INCREMENT COMMENT '前端页面表主键id',
  `page_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '页面的名称,要唯一',
  `page_description` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '页面的功能性描述',
  `action_ids` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '页面对应的操作权限列表，用-连接action的id',
  PRIMARY KEY (`page_id`) USING BTREE,
  UNIQUE KEY `page_name` (`page_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='前端页面表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'dashboard','仪表盘','1-2-3-4-5'),(2,'exam-card','考试列表','1-6-3-4'),(3,'exam-record-list','考试记录','1-6-3-4'),(4,'question-admin','问题管理','1-6-3-4'),(5,'exam-table-list','考试管理','1-6-3-4'),(6,'user','个人页','1-6-3-4-5-7');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question` (
  `question_id` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '题目的主键',
  `question_name` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_bin COMMENT '题目的名字',
  `question_keyword` longtext COLLATE utf8mb3_bin,
  `question_score` int NOT NULL DEFAULT '0' COMMENT '题目的分数',
  `question_creator_id` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '题目创建者的用户id',
  `question_level_id` int DEFAULT '0' COMMENT '题目难易度级别',
  `question_type_id` int DEFAULT '0' COMMENT '题目的类型，比如单选、多选、判断等',
  `question_category_id` int NOT NULL DEFAULT '0' COMMENT '题目的类型，比如数学、英语、政治等',
  `question_description` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_bin COMMENT '题目额外的描述',
  `question_option_ids` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '题目的选项，用选项的id用-连在一起表示答案',
  `question_answer_option_ids` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '题目的答案，用选项的id用-连在一起表示答案',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`question_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='考试题目表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES ('015a392d233d40e5b80223dd7dd49787','团队能持续在产品端到端管理（需求、设计、开发、测试、部署、运营）的每个阶段，每个角色都能从自身领域出发关注市场和竞品的情况，并能产生洞察，将洞见转化到产品规划与设计中。','1',3,'a1b661031adf4a8f969f1869d479fe74',NULL,1,7,'一直如此：团队会围绕产品愿景和目标持续观察市场和用户，做市场分析/竞品分析，并且有洞察，且结果被应用到产品规划与设计中，并使产品某项指标获得显著提升形成闭环。\n经常：针对大多数Feature，团队做了市场分析/用户分析/竞品分析，有洞察，且结果被应用到产品规划与设计中。\n有时：针对少部分Feature，团队做了市场分析/用户分析/竞品分析，有洞察，且结果被应用到产品规划与设计中。\n很少：团队很少做市场分析或竞品分析，或者缺少洞察，结果没有应用到产品规划与设计中并形成闭环','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2020-08-25 04:07:39','2023-02-03 14:58:30'),('08f47737a972452baa0af400944f768f','	产品整体有清晰的北极星指标且团队全员理解一致	','2',3,'a1b661031adf4a8f969f1869d479fe74',0,1,8,'	\"一直如此：团队明确整体产品的北极星指标且全员理解该北极星指标的内容和释义\n经常：团队明确整体产品的北极星指标，超过80%的团队成员能做到“一直如此”\n有时：团队有功能/过程级的增长指标，比较模糊或者不全面/不合理，超过60%的团队成员能做到“一直如此” \n很少：团队不清楚或不理解整体产品的北极星指标，少于60%的情况能做到“一直如此”\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:44'),('0ef428d1eed4404397d5f536282bec70','团队成员是否形成了持续改进的文化，能够持续地定期进行回顾，对各方面的经验和不足进行自我审视，并制定切实可行的、明确了责任人的改进措施，改进过程中关注关键的度量指标；能将改进措施落实到位，并体现在关键的度量指标上。','3',3,'a1b661031adf4a8f969f1869d479fe74',0,2,2,'一直如此：定期回顾，明确责任人与行动项。改进措施透明可视于团队并能准时落地，超过50%的改进体现在关键指标的改变上/n\n经常：团队定期召开回顾，有责任人并大于90%的行动项能按时落地，团队系统地使用关键度量指作为改进参照，超过20%的改进行动项体现在关键指标的改变上/n\n有时：团队定期召开回顾，有责任人并大于60%的行动项能按时落地，团队回顾使用度量指标作为参照，以度量指标的变化作为改进结果目标/n\n很少：团队回顾会召开的频率少于1月一次，或小于60%的行动项能按时落地','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:19:39','2023-02-03 15:05:47'),('104d70807d05482ea438d0b60bdf478e','	产品团队基于用户价值进行的产品设计和迭代，并能对用户感受到价值的方式和效果提出假设；即在需求背景分析阶段，用“提出假设”讲清该需求为用户提供价值的方式	','4',3,'a1b661031adf4a8f969f1869d479fe74',0,1,6,'	\"一直如此：团队能基于用户价值在产品上的表现形式、用户行为都能提出假设，并据此设计功能和运营策略\n经常：对90%的价值点都提出了用户行为的假设\n有时：对60%的价值点都提出了用户行为的假设\n很少：只对核心卖点进行用户行为的假设\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:45'),('16934f014080482fa165eaf6ef0526d9','	在价值评审时，产品团队能否将提出假设、验证假设的逻辑清晰地呈现出来	','5',3,'a1b661031adf4a8f969f1869d479fe74',0,1,6,'	\"一直如此：能够对所有提出的假设使用如用户画像、用户体验地图等形式，将用户价值场景在团队内实现清晰的沟通和呈现\n经常：对80%提出的假设有用户价值场景的呈现\n有时：对50%提出的假设有用户价值场景的呈现\n很少：对50%以内的假设有用户价值场景的呈现\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:45'),('178e630d2c944c03a92da3ba536d54a0','在迭代开始之前，产品经理和团队之间能就故事的验收标准达成一致，保证每一个故事都有清晰的、无歧义的、条目化的验收标准，完整覆盖了业务关心的功能、场景、业务规则及非功能性要求，明确划出一个故事的范围','6',3,'a1b661031adf4a8f969f1869d479fe74',NULL,3,6,'一直如此：所有故事都有明确的验收条件（强制以GWT形式描述），并且在迭代计划会前已经在ALM上持续地记录（在故事描述中或者附件，并且不是在迭代计划会前最后一个工作日才全部提交）；计划会上所有故事高效澄清，能一次性就验收条件达成一致；如果故事在进入迭代后仍有更改，变化比例应小于10%（包括故事增加、删除、描述或验收条件变化）；并在ALM上有明确的修改记录（故事历史、评论区等）。\n经常：一个迭代内60%~90%的用例做到了“一直如此”\n有时：一个迭代内30%~60%以上的用例做到了“一直如此”\n很少：一个迭代内少于30%的用例做到了“一直如此”\n1. 检查Product Backlog中优先级最高的用户/技术故事的验收标准\n2. 检查Sprint Backlog中所有用户/技术故事的验收标准\n3. 观察迭代计划会上需求澄清的效率','c520e4299e534869b6a8de1a76a3c052-50fcf1a6bb1d49de91194be946a3398c-eb278bf1403a4d96b6c958a2d8dbf154-5277b1327e5f496eb2ef74eb3cfad690','5277b1327e5f496eb2ef74eb3cfad690','2020-08-25 04:03:30','2023-02-03 15:05:44'),('1bf6e8ecee59498789c755852c78b05d','	团队有对用户达成北极星指标状态的关键路径进行结构化梳理（如用户旅程/产品流程/用户漏斗）	','7',3,'a1b661031adf4a8f969f1869d479fe74',0,1,8,'	\"一直如此：团队对用户达成北极星指标状态的关键路径有清晰和明确的结构化梳理（包括但不限于产品核心用户旅程/产品流程/用户漏斗）并随产品迭代定期更新\n经常：超过70%的情况能做到“一直如此”\n有时：超过50%的情况能做到“一直如此”\n很少：少于50%的情况能做到“一直如此”\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:44'),('25dbd9aeb47a457e990cf85dd1fa15f0','	建立价值驱动的需求分层管理机制，比如：专题、特性、故事（或其他的术语），明确规范各层需求的定义，并按照需求分层的模式来与团队日常交付活动对齐，构建各层需求对应的反馈机制。	','8',3,'a1b661031adf4a8f969f1869d479fe74',0,1,7,'	\"一直如此：团队能按照各层级需求的周期执行关键活动、各角色都能参与，并使用协作工具执行对于输入、输出的检查。不同层级的需求清单持续动态更新，并有清晰的优先级\n经常：团队针对各层级的需求的关键执行活动、决策活动等都落实到协作工具中。\n有时：团队明确各层需求的定义，团队成员对每一层需求定义和管理方式有充分理解和共识\n很少：团队对于需求的层级没有定义，没有针对各层需求定义关键的执行活动、决策活动等。\n\n分层需求定义举例：\n专题：一个有明确待解决问题和目标的IT投资机会，对应一个完整解决方案 (从0-1设计，重大产品改造)，关注成效指标数据反馈，验证专题价值\n特性：一个独立可交付、用户可用的价值单元，关注版本交付后的用户反馈，验证特性价值\n故事：一个相对独立可开发并得到测试反馈的工作单元，关注测试质量反馈，验证故事价值\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:45'),('2aa0277647dd4ea4ab0c685fd1768a82','	团队中每个开发人员是否及时将代码合并到主干上进行集成	','9',3,'a1b661031adf4a8f969f1869d479fe74',0,3,4,'	特性分支的时间从编码开始的时间计算，而不是从发起PR的时间开始计算	','41a4c6bfaf2249e4950388850576a319-f3a9d4e421bb4d5a8028fea4d8b68e3a-55017c9e2d814b21906ad48b3131b6ff-018d22b3d0cb4e67a9df8d23bdcb6ba9','018d22b3d0cb4e67a9df8d23bdcb6ba9','2023-01-16 11:14:26','2023-02-03 15:05:46'),('31c56e786c1740519606114b5d599acb','	团队是否对测试数据进行管理	','10',3,'a1b661031adf4a8f969f1869d479fe74',0,3,5,'	测试数据的规范管理，能提升测试的效率，能够更快的问题进行复现	','2ef8dfe1a19c4bc3846536f5b3c50322-dcacca977835474da4ce228677f2a604-26fd2597054c45328fb4f77f1697dc30-45f7f6e27def4944ac141cc26eaebb74','45f7f6e27def4944ac141cc26eaebb74','2023-01-16 11:14:27','2023-02-03 15:05:47'),('31cfed31d01b4a018a0e18a202de5c1e','	团队对需求的吞吐量做了度量：团队人均每月上线的故事数，并致力于不断提升单位时间内需求的吞吐量，提升团队的交付效率。	','11',3,'a1b661031adf4a8f969f1869d479fe74',0,1,7,'	\"一直如此：在本班次评估周期內，和上一个评估周期相比，人均故事吞吐量提升超过10%\n经常：在本班次评估周期內，和上一个评估周期相比，人均故事吞吐量提升在5~10%之间\n有时：在本班次评估周期內，和上一个评估周期相比，人均故事吞吐量提升在0~5%之间\n很少：在本班次评估周期內，和上一个评估周期相比，人均故事吞吐量没有提升\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:47'),('39b3a29b438c4ab1a5d2e072c4fa97cb','产品负责人是否为产品树立了明确的愿景和目标，并将该愿景和目标清楚地传达给了团队，例如讲解或张贴在所有人能看到的地方；并为产品的每个发布版本确定了清晰的迭代目标，目标可量化、可验证，并与整个团队共享。','12',3,'a1b661031adf4a8f969f1869d479fe74',NULL,1,8,'一直如此：产品有清晰明确的愿景/目标，制定了可量化的指标，且在团队内进行了对齐，所有人能清晰方向和策略；每个迭代都有基于用户/产品/技术的迭代目标，并进行分头发力\n经常：产品有清晰明确的愿景/目标，制定了可量化的指标、且在团队内进行了对齐，但部分角色无法进行任务拆解或者把日常工作与愿景目标联系起来，对目标进行支撑；大多数迭代都有基于用户/产品/技术的迭代目标\n有时：产品有清晰明确的愿景/目标，不可量化、无法通过数据支撑或者除了产品经理外其他成员不清晰或者在团队内无法达成一致；多数迭代有目标，但是多数迭代目标是交付哪些功能而不是从用户角度出发给能用户带去什么\n很少：产品愿景/目标不清晰，不可量化、无法通过数据支撑、团队内除产品经理，其他成员并不清晰；没有迭代目标或者迭代目标是交付哪些功能而不是从用户角度出发给能用户带去什么','ab1990055ddf4980a9288c350096da6d-86ffc8a5a8534cb1846695853348213d-d780694c60c341a495bc9ed686644464-e05134eff2124345a58004ca501988c3','e05134eff2124345a58004ca501988c3','2020-08-25 04:08:20','2023-02-03 15:05:44'),('3f6db79de9114ebfa8e11028abcfe72a','	代码提交后，能自动化触发流水线，构建成功或失败能立即通知到团队，一旦失败团队能够立即关注并解决导致错误的问题（或回滚代码）让构建通过	','13',3,'a1b661031adf4a8f969f1869d479fe74',0,3,4,'	集成流水线自动触发能够及时对集成的问题进行构建和验证，加快问题反馈	','483d490ecb63467bb68b2204eddeabdd-792fd77e6bcf4423bacd12c5294fdf7c-afab532ac02b41c28e1e7978c820ff15-d54102b4c22c4eff9ecd9a532478777d','d54102b4c22c4eff9ecd9a532478777d','2023-01-16 11:14:26','2023-02-03 15:05:44'),('44f9fa04b17b45fc9e6dc9d49ac514b3','产品人员是否准时参与团队实践活动，包括站会，及Kickoff，Deskcheck工作。产品人员包括：产品经理（数字产品经理）、设计人员（如果需要设计人员的团队）—— 后续出现的产品人员都按照这个定义','14',3,'a1b661031adf4a8f969f1869d479fe74',0,2,1,'一直如此：产品人员（产品经理，设计人员）准时的参与团队所有实践，包含梳理会，计划会，评审会，回顾会，站会，kickoff 与deskcheck。参与这些实践的比例大于90%，只会因为特殊原因缺席/n\n经常：产品人员参与这些实践的比例60%～90%/n\n有时：产品人员参与这些实践的比例30%～60%/n\n很少：产品人员参与这些实践的比例小于30%','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021',NULL,'2023-01-16 09:05:27','2023-02-03 14:07:52'),('4d7e15ef0ea24a97b3d2880e7bd33024','	团队对于关键用户旅程的过程指标有明确且完整的口径定义	','15',3,'a1b661031adf4a8f969f1869d479fe74',0,1,8,'	\"一直如此：团队对于关键用户旅程及产品流程的过程指标有明确且完整的口径定义\n经常：超过70%的情况能做到“一直如此”\n有时：超过50%的情况能做到“一直如此”\n很少：少于50%的情况能做到“一直如此”\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:45'),('4e16da26b0ce4f46afdb6674030793f7','	团队是否具有明确的制品管理及晋级策略，并按照制品晋级策略进行管理	','16',3,'a1b661031adf4a8f969f1869d479fe74',0,3,4,'	制品管理影响到代码的质量、安全，将代码纳入到统一的制品管理并实施明确晋级策略，可保证代码的安全性和可靠性	','25b1c4a4da904c9abc8dcfcbe27b0388-205af17896824711a8ad4ac70ff34025-8de00c5e87eb45a2b48fa7eb61507bd4-c987c8afa5a14baab968d3ae6b63e390','c987c8afa5a14baab968d3ae6b63e390','2023-01-16 11:14:26','2023-02-03 15:05:45'),('501961e1a05c4902912418649d27f92f','	产品团队是否采用设计系统以在保证设计的一致性、延续性的情况下完成快速迭代	','17',3,'a1b661031adf4a8f969f1869d479fe74',0,1,6,'	\"一直如此：设计团队针对不同产品都有统一分模块的设计系统，并能保证复用现成的模块快速调整方案\n经常：有os层面统一的设计系统，可以指导各产品团队独立进行设计迭代\n有时：设计团队有完整、统一、共同维护，设计文件形式的视觉指南\n很少：只能通过评审流程保证设计方案的统一\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:46'),('528f066bd9f7404b99ef45d93e00d5a4','团队的各种会议是否卓有成效，按照5P（Purpose、Product、People、Process、Possible Issues）的方式高效进行。如提前准备充分，有明确聚焦的目标和议程，不延时，参与者积极发言，有清晰的纪要，总能够达成指导行动的成果','18',3,'a1b661031adf4a8f969f1869d479fe74',0,2,1,'一直如此：会议有明确聚焦的目标和议程，会议能够提前做好准备，不延时（按照迭代日历计划准时，按照Scrum活动时间盒的要求），参与者积极发言，有清晰的纪要，总能够达成指导行动的成果/n\n经常：大于60%会议达到 “一直如此”/n\n有时：大于30%会议达到 “一直如此”/n\n很少：小于30%会议达到 “一直如此”','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021',NULL,'2023-01-16 09:05:27','2023-02-03 14:07:53'),('56039b89457346848d0d3efb4357ae6d','团队是否有效运用了物理或电子看板将各种管理信息透明化','19',3,'a1b661031adf4a8f969f1869d479fe74',0,2,1,'一直如此：建立完善的看板机制，能够将以下信息进行可视化，利用看板完成迭代事项的透明、检视和调整，形成闭环，确保迭代版本交付，并进行度量和持续改进\n1. 产品规划路线图/版本计划/n\n2. 团队迭代目标/需求/n\n3. 迭代中的缺陷和问题跟踪管理/n\n4. 迭代中的内外部风险识别和管理/n\n5. 迭代回顾会改进事项/n\n经常：超过90%的情况能做到“一直如此”/n\n有时：超过60%的情况能做到“一直如此”/n\n很少：少于60%的情况能做到“一直如此”','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021',NULL,'2023-01-16 09:05:27','2023-02-03 14:07:53'),('5d8df14abcdf4282932e07161b97c815','	产品人员和团队是否持续地维护一个所有未完成工作的产品待办清单，包括新特性、优化、遗留缺陷和技术改造等工作，所有工作项都按照优先级自上而下进行了排序；	','20',3,'a1b661031adf4a8f969f1869d479fe74',0,1,7,'	\"一直如此：持续地维护所有未完成工作产品待办清单，清单优先级按照自上而下进行排序。根据需求变化及时更新列表，包括新特性、优化、遗留缺陷和技术改造等工作\n经常：梳理会之前，大于90%的事项能达到“一直如此”\n有时：梳理会之前，大于60%的事项能达到“一直如此”\n很少：梳理会之前，小于60%的事项能达到“一直如此”\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:46'),('5f143be5b8ac47a7a84957ef1eff9554','测试人员是否参与需求规划，开发，测试的全流程，并进行Kickoff，Deskcheck的工作','21',3,'a1b661031adf4a8f969f1869d479fe74',0,2,1,'一直如此：测试人员准时的参与团队所有实践，包含梳理会，计划会，评审会，回顾会，站会，kickoff 与deskcheck。参与这些实践的比例大于90%，只会因为特殊原因缺席/n\n经常：测试人员参与这些实践的比例60%～90%/n\n有时：测试人员参与这些实践的比例30%～60%/n\n很少：测试人员参与这些实践的比例小于30%','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021',NULL,'2023-01-16 09:05:27','2023-02-03 14:07:53'),('5f41005125e74208bfe3e81819cfb630','	产品团队是否采用简单、快速的可视化方式在需求阶段能够通过原型展示将需求的价值可视化	','22',3,'a1b661031adf4a8f969f1869d479fe74',0,1,6,'	\"一直如此：频繁、及时地使用原型图辅助澄清需求，配合需求管理等活动帮助团队对方案价值有清晰、统一的认知\n经常：在需求梳理阶段，有80%的项目有原型图的支撑\n有时：在需求梳理阶段，有50%的项目有原型图的支撑\n很少：仅重点项目在需求阶段有原型图的支撑\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:45'),('68b8215b93124b79b73ffe3c039b6631','引入项目协作工具，并起到帮助团队沟通协作的作用','23',3,'a1b661031adf4a8f969f1869d479fe74',0,2,1,'一直如此：团队运用项目协作工具，所有团队成员能够及时的沟通协作。协作工具的信息能做到每日更新，做到信息的准确、状态更新及时。准确的关键效能数据度量完全能够从协作工具中获取。/n\n经常：团队所有成员使用协作工具应用，任务信息更新及时，任务状态更新及时，准确，团队的进度、风险信息可以依赖协作工具获取。但工具的平均使用频率大于1天，有基于协作工具的关键效能数据度量。/n \n有时：协作工具引入，但团队仅部分成员使用，或工具的平均使用频率大于3天，有基于协作数据的团队进度、风险信息的报表。/n\n很少：任何一种协作工具没有引入，或工具的平均使用使用频率大于5天','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021',NULL,'2023-01-16 09:05:27','2023-02-03 14:07:53'),('727a3de6c78d48c0b8fc9f336342871e','开发团队是否采用了TBD分支策略，只有一个长期存在的主干分支（Master），所有特性和修复都要先合并到主干上进行集成。','24',3,'a1b661031adf4a8f969f1869d479fe74',NULL,3,4,'要为特定的整机项目维护不同的分支：\n只要还存在为了某个特定项目维护分支的情况（采用hash发布），哪怕只有一个\n\n要为特定的平台或者OS版本维护不同的分支，或者维护主干之外的开发分支：\n1. 不需要为特定项目维护分支，但需要同时维护两个以上长期（超过一个月）分支（如不同平台基线、不同Android版本基线、不同OS版本基线等）\n2.不管什么原因，出现过大于等于一个迭代的长期分支（典型的情况是每个迭代拉开发分支）\n\n并行的最多只有线上（上一次发布的hotfix）和主干（开发中）的两个分支:\n完全符合TBD分支策略，','e28ced22550e484eac308c3253057b55-56763cf56c5742f0a95a3a5853dcb175-d5232efb29da4790bc0189267557bad2','d5232efb29da4790bc0189267557bad2','2020-08-25 04:06:13','2023-02-03 15:05:44'),('72e0bb2574d649f18716b266d542456e','团队所有角色成员是否能随时顺畅交流，包含产品人员，设计人员、开发人员，测试人员。','25',3,'a1b661031adf4a8f969f1869d479fe74',0,2,1,'一直如此：特性团队成员集中办公，不存在异地或远程办公的情况，团队成员之间的交流可以随时开展，团队内部的问题能够及时响应（2小时内反馈，比如简单信息确认、测试评审、内部问题交流等） 经常：团队成员之间沟通无障碍，可以直接面对面交流，90%以上的问题能够在0.5天内得到响应 有时：团队成员之间沟通不及时，需要中间传递，60%以上的问题1天内才能得到响应 很少：团队成员之间沟通缺失，需要中间传递，30%以上的问题超过1天时间才得到响应。','dbba3b5c93b74f9986e5278e3f53910c-d89e5b9baf534245b227e72fa7fb70ee-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021',NULL,'2023-01-16 07:57:47','2023-02-03 14:07:53'),('77dd4952523941baaa85dca62c9fc0ac','	产品待办清单中高优先级的故事（即下个迭代可能要交付的内容）已经按照INVEST原则拆分到了合适迭代开发的颗粒度	','26',3,'a1b661031adf4a8f969f1869d479fe74',0,1,7,'	\"一直如此：产品待办清单满足远粗近细的原则，高优先级的需求（即下个迭代可能要交付的内容）已经经过梳理和拆分，每一个用户故事已经按照INVEST原则拆分到了合适开发的粒度。合适开发粒度：以迭代时间盒是两周为例，90%的故事为M大小，即开发加测试小于3人天\n经常：大于70%的故事进入迭代前拆分到合适开发的粒度\n有时：大于50%的故事进入迭代前拆分到合适开发的粒度\n很少：小于50%的故事进入迭代前拆分到合适开发的粒度，存在开发加测试超过5人天的故事\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:46'),('79d2811c4ec04211bd9eceb6ea21c347','	在迭代交付时，尽快交付需求并移交测试获得反馈。提升需求的迭代内leadtime：从开始开发到完成团队内部测试（包括修改完该需求相关缺陷的）的周期时间	','27',3,'a1b661031adf4a8f969f1869d479fe74',0,1,7,'	\"一直如此：研发、测试按照故事优先级完成开发、测试工作，超过80%的需求的leadtime < 3天\n经常：研发、测试按照故事优先级完成开发、测试工作，超过50%的需求leadtime < 3天\n有时：研发、测试按照故事优先级完成开发、测试工作，超过50%需求的leadtime < 5天\n很少：研发、测试未按照故事优先级完成开发、测试工作，超过70%的需求的leadtime > 5天\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:45'),('7b2f66bd6f9e45ac9ecc629638da1cb8','	团队是否创建了有效的自动化测试来对组件或系统的功能进行测试（包括接口测试和UI测试），并且这些测试能够通过持续集成来频繁地执行，保证持续有效；而且团队遵循“测试金字塔”原则，不同层级的测试有合适的覆盖范围	','28',3,'a1b661031adf4a8f969f1869d479fe74',0,3,5,'	\"自动化测试集通过率应达到90%，如没有达到视为自动化测试无效\n冒烟功能/接口测试应至少覆盖5%的功能/接口测试用例\n核心功能/接口测试应至少覆盖10%的功能/接口测试用例\n1.检查自动化测试报告（流水线、O测），分析测试数量变化趋势\n2.抽查自动化测试有效性（有断言、无复杂逻辑、被测对象合理）\"	','dde527246b084992a8ea15626e1b8e4d-3e7137452fe845b0980f56697447d8f6-3f0bc4a3cc86405badc63edad88643a7-707f5f38348144eba6af2dc40100e18f','707f5f38348144eba6af2dc40100e18f','2023-01-16 11:14:27','2023-02-03 15:05:46'),('7f591c4e8bb34d06ac3eb37ae506379d','	团队是否有统一的编码规范，并按照编码规范的要求进行执行	','29',3,'a1b661031adf4a8f969f1869d479fe74',0,3,5,'	需要制定团队级的代码规范，并结合自动化工具进行落实	','a95882064dce45dfab534d0bfc4d9011-45d33a01ae934e76b88cc3a1c2e396e2-69350aff99944f93be566eaac8298e11-4753a830a38d4b8d8b4d8506950bf3d1','4753a830a38d4b8d8b4d8506950bf3d1','2023-01-16 11:14:27','2023-02-03 15:05:45'),('8231ec6503a4485b8cb4828d07bf76ea','	团队是否严格遵循TBD分支策略所需的流水线配置：Master流水线用于主干分支上的验证（包含静态扫描、单元测测试、冒烟自动化）、Release流水线用于版本发布（包含静态扫描、单元测测试、冒烟自动化）、Policy流水线用于PR（包含静态扫描、单元测测试、冒烟自动化）、Feature分支用于非主干分支的提交验证（包含静态扫描、单元测测试）。	','30',3,'a1b661031adf4a8f969f1869d479fe74',0,3,4,'	\"还有手动编译交付件的情况：\n进入测试、互动、灰度、发布等环节需要使用的版本有些时候还要通过手动拉取代码编译，而不是直接从制品服务器上取\n\n所有构建都使用流水线自动完成，但流水线上验证不合理或者不完整：\n所有流水线的提交都有自动化流水线完成，但流水线基本只能完成编译、打包，不包含静态扫描、自动化测试等验证，或者包含的静态扫描、自动化测试验证无效（如失败不会终止流水线、扫描规范被部分屏蔽、自动化测试数量不够、质量不高）\n\n所有构建和发布都使用流水线自动完成且流水线上验证合理完整：\n流水线可以自动化打通全部发布渠道（项目版本火车、SAU、商店等）；根据不同的分支的要求，采用了不同配置的流水线来保护所有分支：“特性分支”上只做最快速的验证（编译+单元测试）、PR到保护分支前必须保证流水线通过（编译+单元测试+冒烟测试+静态扫描）、保护分支集成流水线（编译+单元测试+冒烟测试/接口测试+静态扫描+打包）、发布流水线（编译+单元测试+功能测试/接口测试+静态扫描+打包+发布+tag）。上述流水线中的各种自动化测试应足够多并且有效（单元测试数量至少上百或者测试覆盖超过20%，冒烟测试应涵盖至少5%的手工用例）。上述流水线中的静态扫描可能由于性能问题放到独立流水线扫描。\n\n遵循TBD分支策略所需的流水线配置，并对流水线的构建进行了优化调整：\n完全符合TBD分支策略要求的流水线配置和完整的验证。所有流水线的平均构建时长在20分钟以内（包括静态代码扫描在内）\n\n以上每条流水线执行成功率应达到最近一周80%，如没有达到视为无效i流水线\"	','3e444a901c0a403d9650747e573814d6-d1390bc382c045ab9662bb50d6f29f37-7ea5fe5dc5cc43aa959428670524b807-de106849ca2641b3927542ac81392d6b','de106849ca2641b3927542ac81392d6b','2023-01-16 11:14:26','2023-02-03 15:05:46'),('875fe33b87444f0086e893cf9aa2ec16','是否将产品、团队、过程、技术等有价值的信息有效地沉淀下来，让所有成员能够容易地获取','31',3,'a1b661031adf4a8f969f1869d479fe74',0,2,2,'一直如此：产品、团队、过程、技术等有价值的信息有效地沉淀下来，让所有团队成员能够方便对获取。不会因为关键角色的变动会造成价值信息的丢失/n\n经常：超过90%的价值信息都有沉淀，只有极少部分关键信息需口头传递/n\n有时：超过60%的价值信息都有沉淀，部分关键信息还需口头传递/n\n很少：少于60%的价值信息有沉淀，更多的是依靠口头传递，关键角色的变动会造成价值信息的丢失','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:19:39','2023-02-03 15:05:46'),('88247243fedb4241b93ce565a4db1d6d','	是否对关键架构设计进行识别并对架构设计方案进行评审	','32',3,'a1b661031adf4a8f969f1869d479fe74',0,3,3,'	\"对新功能新特性或重大的技术架构改造，进行技术方案的概要设计和评审，对技术风险和实现进度进行识别\n一直如此：\n所有故事都有明确的技术决策（简单故事）/技术方案（复杂故事或技术故事），并且在迭代计划会前已经在ALM上持续地记录（在故事描述、讨论区中以文本、链接等形式记录，并且不是在迭代计划会前最后一个工作日才全部提交）；计划会上所有技术决策/方案高效澄清，能一次性达成一致；在迭代内技术方案/决策变更的故事不应超过10%，并以明确的变更记录在ALM上；技术方案以代码、Wiki、CF等形式长期持续维护，并有维护更新的记录。\n\n经常\n一个迭代内60%~90%的用例做到了“一直如此”\n\n有时\n一个迭代内30%~60%以上的用例做到了“一直如此”\n\n很少\n一个迭代内少于30%的用例做到了“一直如此”\n\n1. 检查Product Backlog中优先级最高的用户/技术故事的技术决策/方案\n2. 检查Sprint Backlog中所有用户/技术故事的技术决策/方案\n3. 观察迭代计划会上技术决策/方案澄清的效率\"	','efdbc47f07ef4608924c78d95320eef2-17f395ffe93b45eaaaf7d1580a2b0ee6-be3d04b7a4484e3a96fce8718c623bad-dde75d5c3d8741aabe971235e044f81e','dde75d5c3d8741aabe971235e044f81e','2023-01-16 11:14:26','2023-02-03 15:05:45'),('8d7b5ca75a5748d6bb93fb150993a5b3','	团队开发过程中，测试工作是否提前介入并与开发人员进行协同	','33',3,'a1b661031adf4a8f969f1869d479fe74',0,3,5,'	测试工作需要提前介入，在开发过程中与开发人员同步测试用例，使开发人员能够提前验证用例，及时做出改进	','1af594bc97554d44b868dd388ade142c-255d517a6e464c73a6e684e076346d0e-ccd8bbb3d10a4fc5b686ec16e8d84ca7-2f845e0c2d7a4c018fb2aff73ccfe27b','2f845e0c2d7a4c018fb2aff73ccfe27b','2023-01-16 11:14:27','2023-02-03 15:05:46'),('920e56d5cdeb4813ad9b2ddf7154a27f','	需求的描述需要包含验收标准（AC）。验收标准被用作：需求工作量评估的主要输入；研发开始开发前和产品确认需求理解清晰、一致的基础；研发将需求移交测试前获得产品经理、测试反馈的主要依据。验收标准建议用Given-When-Then的方式描述	','34',3,'a1b661031adf4a8f969f1869d479fe74',0,1,7,'	\"一直如此：所有需求都包括验收标准，验收标准会被作为测试、产品经理、研发沟通需求定义、评估需求大小、编写测试用例的主要依据。\n\n经常：90%以上的需求包括了验收标准，并作为需求开始开发、移交测试时产品经理、测试、研发沟通需求定义的主要依据\n\n有时：50%以上的需求包括了验收标准。\n\n很少：没有验收标准，需求以非结构化形式描述，在单个用户故事/需求正式开始开发前、移交测试没有产品经理和研发澄清、确认的过程\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:47'),('935dbb6e4919464da0537ace35b27d66','	架构师或技术负责人是否承担了重要的编码工作，如主体框架搭建、新技术预研、技术难点攻关、核心业务逻辑实现、共用代码封装等等。	','35',3,'a1b661031adf4a8f969f1869d479fe74',0,3,3,'	\"架构师的代码贡献要求如下（满足以下全部条件）：\n1. 架构师提交的代码应该在产品中有关键作用，包括但不限于\n1.1. 技术预研类代码（有对应技术故事，且描述清楚）\n1.2. 架构框架类代码（有对应架构描述，文档或者Wiki形式）\n1.3. 核心业务类代码（有对应故事或设计文档，且优先级最高）\n1.4. 公共类库类代码（有对应技术故事，且描述清楚）\n2. 代码通过PR合并到Master上并通过流水线检查\n\n如团队中有多人承担架构师角色，贡献频率则按人均计算\"	','1e9003997cd74939b2b54522efb2ff5d-907d68280d954c55b024b27f94ca26ca-378939f1684946749c21eb3ab0e70f16-bcba47f4c69d434eabc1e2b3521ac07d','bcba47f4c69d434eabc1e2b3521ac07d','2023-01-16 11:14:26','2023-02-03 15:05:46'),('941a7702e89f460c9f0746e257ac5613','团队是否能够积极尽早地识别和管理风险，比如采用风险管理矩阵，或在故事地图、迭代看板上显式地标注了外部依赖和不确定性等风险和问题，在每次的计划和评审过程中对风险和问题进行讨论并更新；及时考虑和采取了有效措施来应对风险','36',3,'a1b661031adf4a8f969f1869d479fe74',0,2,1,'一直如此：团队积极尽早的识别和管理风险，采用风险管理矩阵，或在故事地图与迭代看板上标注外部依赖及风险。对识别风险积极应对并定期追踪更新。大于80%的风险能在迭代前发现，并采取了相应的应对措施/n\n经常：大于90%的概率做到“一直如此”/n\n有时：大于60%的概率做到“一直如此”/n\n很少：小于60%的概率做到“一直如此”','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021',NULL,'2023-01-16 09:05:27','2023-02-03 14:07:54'),('9a88883749f34d8dafcd0d163f48e1bb','	开发人员是否在充分完成自测后，在开发环境上给测试人员和产品人员进行当面演示（即Deskcheck），在验收标准和测试场景都通过的前提下转测，由测试人员再做完整故事测试	','37',3,'a1b661031adf4a8f969f1869d479fe74',0,3,5,'	\"一直如此：迭代内故事一次性通过率超过90%\n经常：迭代内故事一次性通过率超过70%\n有时：迭代内故事一次性通过率超过50%\n很少：迭代内故事一次性通过率低于50%\n观察团队的Deskcheck活动，是否由开发按照验收标准和测试用例进行演示（包括按照产品和测试要求简单探索，以及自动化测试执行结果）\"	','8c3d3721c67a455fbe6bd4485bc6ef58-b540053569434733897d30d353cc6b25-05fa17a379764e4e80bf4d600c3da752-0c7265efc30644479492b9afa4c97007','0c7265efc30644479492b9afa4c97007','2023-01-16 11:14:27','2023-02-03 15:05:47'),('9df47949d02e410091d2705053cde6ed','团队建立可持续的迭代节奏，时间盒<1个月。团队根据自身实际容量和历史迭代速率制定可行的、可承诺的迭代计划，不会过度承诺或承诺不足。迭代故事完成率为','38',3,'a1b661031adf4a8f969f1869d479fe74',0,2,2,'一直如此：迭代按照时间盒定义，迭代任务列表中的故事完成率大于90%/n\n经常：迭代按照时间盒定义，迭代任务列表中的故事完成率在71~90%之间/n\n有时：迭代按照时间盒定义，迭代任务列表中的故事完成率在51~70%之间/n\n很少：迭代按照时间盒定义：迭代不是按照周期性开始、结束时间定义，每个迭代的任务列表在迭代过程中的增减没有按照事先定义的优先级规则','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:19:39','2023-02-03 15:05:46'),('9f0389beeea044098061ea256ffaeba1','	产品有定制化的细分增长指标仪表盘	','39',3,'a1b661031adf4a8f969f1869d479fe74',0,1,8,'	\"一直如此：产品有定制化的细分增长指标仪表盘（监测用户行为和关键指标）\n经常：超过70%的情况能做到“一直如此”\n有时：超过50%的情况能做到“一直如此”\n很少：少于50%的情况能做到“一直如此”\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:46'),('a37c3526f1ae49a4a3a77c16c7e32607','团队是否能遵循迭代日历的节奏，按时、完整地进行各项关键迭代活动，包括待办清单梳理和估点，迭代计划，站会，迭代评审演示，迭代回顾会','40',3,'a1b661031adf4a8f969f1869d479fe74',0,2,1,'一直如此：团队有制定合理的迭代日历，每个迭代包含迭代梳理会，计划会，站会，回顾会，评审会，code review的计划安排。遵循迭代日历的节奏，大于90%迭代活动能按时、完整地进行\n经常：遵循迭代日历的节奏，60～90%迭代活动能按时、完整地进行\n有时：遵循迭代日历的节奏，30～60%迭代活动能按时、完整地进行\n很少：没有迭代日历，或小于30%迭代活动能按时、完整地进行','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021',NULL,'2023-01-16 08:34:57','2023-02-03 14:07:55'),('a5df7c06a7e942d189a68b325613d5db','	开发团队是否采用了TBD分支策略，只有一个长期存在的主干分支（Master），所有特性和修复都要先合并到主干上进行集成。	','41',3,'a1b661031adf4a8f969f1869d479fe74',0,3,4,'		','b7bfc6ef6bcb4519b1b01cd7a1a09172-098fde31a03f407fa94bcf0e86f8df56-e3a3e23924614d038bdc9a2563b06e8a-5c91884aae70446b93e5c8a9ef884ad6','5c91884aae70446b93e5c8a9ef884ad6','2023-01-16 11:14:26','2023-02-03 15:05:46'),('a5e81890180d4be0ae8712b9bc6ed41c','团队内部或相关的多个团队之间是否有进行定期的交流和分享，将经验和教训在组织中横向进行传播；','42',3,'a1b661031adf4a8f969f1869d479fe74',0,2,2,'一直如此：团队内或团队间有定期的交流和分享，将经验和教训在组织中横向的传播。定期分享频率1周超过1次/n\n经常：团队内或团队间的定期分享频率大于1周/n\n有时：团队内或团队间的定期分享频率大于2周/n\n很少：没有定期的团队内或团队间的分享，或分享频率大于1个月','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:19:39','2023-02-03 15:05:45'),('abefb1996d4844dfac020237bf69991d','	在需求价值衡量阶段，产品团队能根据所提出的假设，设计验证假设的实验和反馈方式，保证提出的假设可被验证和衡量，并以此作为价值评判的主要基准	','43',3,'a1b661031adf4a8f969f1869d479fe74',0,1,6,'	\"一直如此：产品团队能够对所有提出的假设设计验证实验和预期的定性或定量的结果\n经常：对80%提出的假设设计验证实验和预期的定性或定量的结果\n有时：对50%提出的假设设计验证实验和预期的定性或定量的结果\n很少：对50%以内的假设设计验证实验和预期的定性或定量的结果\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:45'),('ac8afe6ff57c4246947f396da6bec63c','	团队中是否具有明确的架构设计规范（原则）	','44',3,'a1b661031adf4a8f969f1869d479fe74',0,3,3,'	\"1、制订架构设计原则，如：简单、优雅、运营端/用户端操作人性化 等\n2、按照架构设计原则审视交互、策划、视觉、概设，对于违背架构设计原则的设计坚决驳回\n3、阶段回溯、审视架构设计问题并进行复盘、修正\n基本规范：具备架构设计的基本原则，核心要素，\n详细规范：除基本规范外，具有完备的要素说明、示例，应对不同场景的详细说明、反例等\"	','73ef86651be949c7a9f748dce8d54d81-ba7f599562884999ab279c79820a6ae7-88b6e22038884799b7f58f997f8911fb-bc9a0957c0ae451c984dee4e1ea4b74a','bc9a0957c0ae451c984dee4e1ea4b74a','2023-01-16 11:14:26','2023-02-03 15:05:45'),('ad7e05a6f421448092e43a411f5c087e','	团队有明确的需求价值评估模型（根据比如商业/业务/用户/技术，上市时间和成本等综合决策），并能依据该模型进行优先级排序；当有新的需求进到产品待办列表时，始终进行优先级排序	','45',3,'a1b661031adf4a8f969f1869d479fe74',0,1,7,'	\"一直如此：团队有明确的需求价值评估维度（根据商业/业务/用户/技术，上市时间和成本等综合决策），当有领导/个别用户/需求提出方提出不同意见时，也能够按照该模型评进行科学评估；每当团队有新需求被加到需求代办列表时，需要对此需求进行业务/用户/技术侧的完整评估，交付价值而非交付单一功能。\n经常：团队有明确的需求价值评估维度，70%的情况能做到“一直如此”\n有时：团队有价值评估维度，比较模糊或者不全面/不合理，50%的情况能做到“一直如此”\n很少：团队没有价值评估维度或者评估维度单一，70%的情况做不到“一直如此”\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:46'),('b1c4b6393e0d46338180a4a1017456f5','是否具备跨职能的全功能团队 （产品人员，开发人员，测试人员），所有人在迭代中能全程参与 产品人员：包含产品经理，设计人员（如果需要设计人员的团队）','46',3,'a1b661031adf4a8f969f1869d479fe74',NULL,2,1,'跨职能的全功能团队：\n产品人员：包含产品经理，设计人员（如果需要设计人员的团队）\nTL：技术负责人\nSM：Scrum Master\n测试：测试人员 （至少一名全职）\n一直如此：特性团队具备跨职能的全功能团队，以上提到的所有角色全职参与特性团队端到端实践，支持产品的价值交付\n经常：以上任一角色非全职投入到特性团队\n有时：以上任一角色只有少于60%的精力投入到特性团队\n很少：以上任一角色只有少于30%的精力投入到特性团队','db89b05b80d543eb8fb0870bb5650948-3a7684eb0e3d4bf78b87f2c4cf371666-5e5bc2dbb84743b1ba615ef22245f4f6-ed3b5b8a06544ec5b7d81b62879719e1','ed3b5b8a06544ec5b7d81b62879719e1','2020-08-25 03:49:26','2023-02-03 14:07:55'),('bf102c56d9a6407289ba1cd9b11b4ccb','全功能团队的团队大小为','47',3,'a1b661031adf4a8f969f1869d479fe74',0,2,2,'','d01d4873706d43de8578f40e7f5a2ca4-e78a8c28d2d04d0fbea1c22ac44e22e8-076d9f3e3e1f42e4b9c93128348ccceb-805e732a65ba4bedaf31925ec5a4e928','805e732a65ba4bedaf31925ec5a4e928','2023-01-16 09:19:39','2023-02-03 15:05:46'),('bff114d05ccd40a1937db177f7262e95','	面客类产品定期会对用户进行精细化分群或分层管理	','48',3,'a1b661031adf4a8f969f1869d479fe74',0,1,8,'	\"一直如此：面客类产品（不含对内型产品）定期会对用户进行精细化分群或分层管理\n经常：超过70%的情况能做到“一直如此”\n有时：超过50%的情况能做到“一直如此”\n很少：少于50%的情况能做到“一直如此”\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:47'),('c117c70ecdd14970b31c544ed36a6fcc','	团队成员是否密切关注了持续构建的问题并进行及时的修改，对环境问题能够及时修复或者回滚	','49',3,'a1b661031adf4a8f969f1869d479fe74',0,3,4,'	\"基本检查有lint、coverity、findbugs（以部门要求的规范为准），如果有忽略的规则应有备案\n代码坏味道主要有方法复杂度和代码重复率（sonar正在实施中，但可以本地扫描）\n\n仅仅关注最基本的检查，等门禁触发后才修改：\n以部门统一要求的标准扫描，任意流水线还存在经常扫描不通过的情况（超过每周一次）\n\n仅仅关注最基本的检查，在提交前就尽可能杜绝:\n以部门统一要求的标准扫描，任意流水线极少出现扫描不通过的情况（不超过每周一次）\n如果基本检查没有做到每日检查不能选此选项\n\n关注代码坏味道等门禁触发后才修改：\n部门统一要求的标准扫描之外，还关注sonar的复杂度和重复率等指标，任意流水线还存在经常扫描不通过的情况（超过每周一次）\n如果代码坏味道没有做到每日检查不能选此选项\n\n关注代码坏味道，在提交前就尽可能杜绝：\n部门统一要求的标准扫描之外，还关注sonar的复杂度和重复率等指标，任意流水线极少出现扫描不通过的情况（不超过每周一次）\n如果代码坏味道没有做到每日检查不能选此选项\"	','73ef86651be949c7a9f748dce8d54d81-ba7f599562884999ab279c79820a6ae7-88b6e22038884799b7f58f997f8911fb-bc9a0957c0ae451c984dee4e1ea4b74a','bc9a0957c0ae451c984dee4e1ea4b74a','2023-01-16 11:14:26','2023-02-03 15:05:46'),('c348c930dba64c01be1e54ab080b1f2d','	架构师或技术负责人，作为团队的一份子，参与团队各种关键活动；	','50',3,'a1b661031adf4a8f969f1869d479fe74',0,3,3,'	\"架构师的架构、设计和实现工作包含：\n1. 提前参与需求分析梳理，识别技术风险，并与PO达成一致\n2. 重大技术方案设计决策，其他技术方案Review\n3. 代码Review\n4. 核心代码贡献（参考下一个问题的释义）\n5. 技术债务识别和跟踪，就偿还技术和PO达成一致\n6. 质量内建度量跟踪分析以及实践推广落地\n7. 前沿技术研究以及推广\n8. 技术强相关会议\n\n架构师不应过多投入的工作：\n人员管理、任务分配、非技术强相关会议等\n\n绝大部分时间都在做架构、设计和实现相关工作：90%以上工作时间\n部分时间都在做架构、设计和实现相关工作：50%以上工作时间\n很少时间在做架构、设计和实现相关工作：不足50%工作时间\n\n如果团队有多位架构师，每位架构师都要满足上述要求才能得分\"	','d7b2476e4ddc49729391d21c6e31fc2e-d6f8a0c07bce45c4b002e08f678107ff-216f96dea9504f08bdafb361e04edde4-98ef90d88b7b4e9285c26fdfa9762f54','98ef90d88b7b4e9285c26fdfa9762f54','2023-01-16 11:14:26','2023-02-03 15:05:45'),('c3a9f0c044264f73a4436a7ed91941d2','当一个较大的产品由多个紧密关联的团队构成时，是否存在有效的机制（比如Scrum of Scrums会议）在多个团队之间进行例行的沟通协调，相关干系人都能参加，积极管理彼此的依赖和影响，做到计划和进度对齐。既保持各团队相对独立运作，又确保跨团队的工作能够及时高质量交付','51',3,'a1b661031adf4a8f969f1869d479fe74',0,2,1,'一直如此：多个紧密关联的项目团队，采用定期有效机制在多个团队例行沟通协调，相关干系人参加。能保证以下所有事项的顺利进行/n\n1. 关联功能特性的版本对齐/n\n2. 依赖与风险提前识别与应对/n\n3. 计划和进度对齐/n\n4. 关联特性发布计划对齐/n\n经常：90%以上的情况能做到“一直如此”/n\n有时：超过60%能做到“一直如此”/n\n很少：少于60%能做到“一直如此”','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021',NULL,'2023-01-16 09:05:27','2023-02-03 14:07:56'),('c65afaa22d5243db9c20230953fd5cc4','	团队是否每天关注工程效能的度量数据，并进行改进	','52',3,'a1b661031adf4a8f969f1869d479fe74',0,3,4,'	\"对于不同的度量指标，采用不同的关注频率和改进措施\n1. 对于过程指标，需要每日持续关注，并及时调整\n2. 对于结果指标需要进行迭代回顾，并转换为下阶段的改进活动\"	','9293da27354d44be989da92e0ddcce16-4e0fb5bec14646cabdb9ff211a88f4b6-e2771033cab54d45ba66c14aa6f7649c-49db849af3554873a36786498b12fd08','49db849af3554873a36786498b12fd08','2023-01-16 11:14:27','2023-02-03 15:05:45'),('cb2d4a08df4a430a83c1c48a1366385c','	团队是否对当前的技术架构进行可视化	','53',3,'a1b661031adf4a8f969f1869d479fe74',0,3,3,'	\"1. 对团队的应用架构进行可视化，如：通过C4模型展示不同层次的架构视图。\n2. 架构视图能够让团队成员方便的看到，并能够随时对架构进行讨论\"	','87011881e47b496c9c7bde573da38dea-c9318a860d83403ea2457e7749247e12-28e933e61d3b4a9183f11b16cafbe9b6-d7a116d9d8e443a297953beb33497745','d7a116d9d8e443a297953beb33497745','2023-01-16 11:14:26','2023-02-03 15:05:47'),('d1f631a3e83e41678f8c8e2fec4a4c93','	团队是否建立了每天例行的代码评审机制，对每天提交代码的质量进行集体评审，并有机制确保评审发现的问题都进行了修复	','54',3,'a1b661031adf4a8f969f1869d479fe74',0,3,5,'	\"集体代码检视要求如下：\n1.时间基本固定（80%都发生在同一时间）\n2.时间长度不超过1小时\n3.全体开发人员如无不可抗拒的原因，必须到实地\n4.检视过程中发现的问题有记录有追溯（查看Wiki、CF或其他记录）并在活动上线进行回顾\n5.集体代码重点问题在于共享的知识（模式、反模式等），在记录问题中的比例超过70%\n\n如果不满足上述要求任意一项，则视为集体代码检视活动无效。\"	','500cc849186d4da3b65eac739b72a949-90f528cff8a2427d907032ca0eb6d8e3-3124a840c5d442dbbebe85f94c966242-d7e17c36292e464486cb90e01ca23d4d','d7e17c36292e464486cb90e01ca23d4d','2023-01-16 11:14:27','2023-02-03 15:05:45'),('d1f8fef52d8f46308414cda76a766ed8','是否具备跨职能的全功能团队 （产品人员，开发人员，测试人员），所有人在迭代中能全程参与','55',3,'a1b661031adf4a8f969f1869d479fe74',0,2,2,'跨职能的全功能团队：\n产品人员：包含产品经理（数字产品经理），设计人员（如果需要设计人员的团队）/n\nTL：技术负责人/n\nSM：Scrum Master/n\n测试：测试人员 （至少一名全职）/n\n一直如此：特性团队具备跨职能的全功能团队，以上提到的所有角色全职参与特性团队端到端实践，支持产品的价值交付/n\n经常：以上任一角色非全职投入到特性团队/n\n有时：以上任一角色只有少于60%的精力投入到特性团队/n\n很少：以上任一角色只有少于30%的精力投入到特性团队','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:19:39','2023-02-03 15:05:45'),('d2e8339aa8e04e36afd253a5be3b0d72','	团队是否具备的完善的环境，并能对环境进行快速的重建。如：开发环境、测试环境、预发布环境、生产环境等	','56',3,'a1b661031adf4a8f969f1869d479fe74',0,3,4,'	\"1. 完备的环境能够使开发过程减少相互干扰，不同的角色关注特定的环境进行工作。\n2. 方便制品晋级并构建出一个可稳定发布的制品\"	','e3bd9c02871e469b938af2b41268ef2a-1fafd621f97c4a15ae2196cc900ed5ec-29794c3b08854507ba9036d78639c246-227d18f26e5147d08153b666b6061036','227d18f26e5147d08153b666b6061036','2023-01-16 11:14:26','2023-02-03 15:05:46'),('dc08433bb7604e90aa1ad0ea33845bde','团队成员参与、认可团队的整体的OKR目标设定，将团队目标和日常的工作建立了清晰的连接，并在过程中按照目标实现的状态和进度适时调整','57',3,'a1b661031adf4a8f969f1869d479fe74',0,2,2,'一直如此：团队核心成员共同参与设定团队OKR，在业务目标聚焦的前提下，充分考虑能力成长、技术债偿等维度。除了将OKR分解为季度OKR、还有月度里程碑、迭代目标等。团队的迭代工作成果关联并且反映到OKR的进展。/n\n经常：团队将迭代工作成果体现在OKR的进展里，团队定期审视月度里程碑、迭代目标和OKR之间的关联和进展。/n\n有时：OKR的分解、KR的设定考虑了团队多个职能的输入，KR的达成是团队日常工作一部分，并且周期性的审视OKR的达成情况并修正KR。/n\n很少：团队OKR、季度OKR由一两个人分解完成，只是组织OKR的简单数字分解，O和KR之间是简单的分解，和团队实际工作没有发生关联','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:19:39','2023-02-03 15:05:46'),('e20f61aaba3b4e7caeb365286954c4ce','团队共同制定工作量的估算进行集体估点，而非领导决策/个人决策','58',3,'a1b661031adf4a8f969f1869d479fe74',NULL,2,2,'一直如此：所有开发团队人员（开发人员，测试人员）参与工作量估算进行集体估点，估算过程不会受到领导或专家角色经验与权威的干扰。团队可以根据估算共同决策，承诺迭代范围\n经常：90%以上故事能达到“一直如此”\n有时：60%以上故事能达到“一直如此”\n很少：低于60%以上故事能达到“一直如此”','ca0bacf113044a87bf0f6027112967c2-523f2172ec964c73a02eb65963eb362f-63e23813ca204e97ae3beade42299f30-716f8c4995a54f8395d1a9842e36f8fe','716f8c4995a54f8395d1a9842e36f8fe','2020-08-25 03:58:02','2023-02-03 15:05:44'),('e5e93172384c47f48e434160bed181fe','	团队是否对代码层面的技术债进行管理，并持续减少技术债	','59',3,'a1b661031adf4a8f969f1869d479fe74',0,3,5,'	为了使代码具有更好的可读性和可维护性，对于静态扫描的技术债，需要进行持续的关注，并持续解决技术债	','613080cfc0444d6eaf17d51ed9f164c1-a5150303ade24e36b5f740c7477258a7-e8cb5ddd2fa24b44b3ad093b8b6d5ed6-e3c23e899eb94c00979d04a77e4a4100','e3c23e899eb94c00979d04a77e4a4100','2023-01-16 11:14:27','2023-02-03 15:05:45'),('ef4290f39e4c4e57b6e3a2323cddcfdb','不存在知识和技能的单点瓶颈，团队可以互相技能备份','60',3,'a1b661031adf4a8f969f1869d479fe74',0,2,2,'一直如此：团队不存在单点瓶颈，人员技能做到完全的备份，包括产品人员、开发人员和测试人员，不会因为人员或角色的缺席导致问题的阻晒/n\n经常：团队有一定的技能备份，60%-90%的问题（原有特定团队中领域人员才能解决的问题）可以自己解决，不会影响到迭代目标的达成/n\n有时：团队有一定的技能备份，30%-60%的问题（原有特定团队中领域人员才能解决的问题）可以自己解决，可能会影响到迭代目标的达成/n\n很少：团队有一定的技能备份，小于30%的问题（原有特定团队中领域人员才能解决的问题）可以自己解决，可能会影响到迭代目标的达成','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:19:39','2023-02-03 15:05:46'),('f4a724372efc41f4b76efc4cb610ae3e','	团队定期会通过数据分析找到增长线索提出增长假设策略	','61',3,'a1b661031adf4a8f969f1869d479fe74',0,1,8,'	\"一直如此：团队定期会通过数据分析找到增长线索或转化断点并科学地提出增长假设策略\n经常：超过70%的情况能做到“一直如此”\n有时：超过50%的情况能做到“一直如此”\n很少：少于50%的情况能做到“一直如此”\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:44'),('fc048e5dbc7b45a2a76a0b36dc253549','	团队经常通过数据采集追踪用户行为数据	','62',3,'a1b661031adf4a8f969f1869d479fe74',0,1,8,'	\"一直如此：团队经常通过数据采集追踪用户行为数据（制定数据追踪方案，收集用户行为及产品转化数据）\n经常：超过70%的情况能做到“一直如此”\n有时：超过50%的情况能做到“一直如此”\n很少：少于50%的情况能做到“一直如此”\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:46'),('fc4cfd48c128469dbdf8a7620856ff29','	团队是否对架构技术债进行管理	','63',3,'a1b661031adf4a8f969f1869d479fe74',0,3,3,'	\"1. 对架构设计中的遗留问题进行记录和整理\n2. 对需要进行架构改造的问题进行优先级排序，并进行工作任务排期\n很少：几乎没有，偶尔\n有时：半年-一年时间一次\n经常：一个季度内\n一直如此：每个迭代\"	','cf992be5d6a34a17801d8b69fe93da97-12e7a3cf71964e09acdfa504af761b1a-e5cab4ca0da74291bc92eaae041417d5-8f0ac1927722494483fdc77e39ea163f','8f0ac1927722494483fdc77e39ea163f','2023-01-16 11:14:26','2023-02-03 15:05:45'),('fd23eb882224422595356366b8de7198','	产品需求按照故事的形式描述，进入迭代前能够满足DOR，包括需求计划阶段要明确需求（用户、痛点、解决方案的）的假设、假设的验证方法，需求设计阶段充分发现关键的依赖、可行性问题以及验收标准。	','64',3,'a1b661031adf4a8f969f1869d479fe74',0,1,7,'	\"一直如此：团队对与DOR有明确对定义，大于90%的故事进入迭代时能够满足DOR\n经常：团队对于DOR有明确对定义，60%～90%的故事进入迭代能够满足DOR\n有时：团队对于DOR有明确对定义，30%～60%的故事进入迭代能够满足DOR\n很少：团队没有定义DOR，或小于30%的故事进入迭代能够满足DOR\"	','d89e5b9baf534245b227e72fa7fb70ee-dbba3b5c93b74f9986e5278e3f53910c-29277d4c4fd34981b68c8fcec493e44b-9d24347e52374a26938c749f5abbe021','29277d4c4fd34981b68c8fcec493e44b','2023-01-16 09:51:01','2023-02-03 15:05:45');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_category`
--

DROP TABLE IF EXISTS `question_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question_category` (
  `question_category_id` int NOT NULL AUTO_INCREMENT COMMENT '问题类别表的主键',
  `question_category_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '问题类别名称',
  `question_category_description` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '问题类别的描述',
  PRIMARY KEY (`question_category_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='题目类别表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_category`
--

LOCK TABLES `question_category` WRITE;
/*!40000 ALTER TABLE `question_category` DISABLE KEYS */;
INSERT INTO `question_category` VALUES (1,'沟通协作','沟通协作'),(2,'团队治理','团队治理'),(3,'架构设计','架构设计'),(4,'持续集成','持续集成'),(5,'质量保障','质量保障'),(6,'设计驱动','设计驱动'),(7,'需求管理','需求管理'),(8,'持续增长','持续增长'),(9,'持续发展','持续发展');
/*!40000 ALTER TABLE `question_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_level`
--

DROP TABLE IF EXISTS `question_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question_level` (
  `question_level_id` int NOT NULL AUTO_INCREMENT COMMENT '题目难易度的主键',
  `question_level_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '题目难易度名称',
  `question_level_description` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '题目难易度的描述',
  PRIMARY KEY (`question_level_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='问题的难易度级别';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_level`
--

LOCK TABLES `question_level` WRITE;
/*!40000 ALTER TABLE `question_level` DISABLE KEYS */;
INSERT INTO `question_level` VALUES (1,'high','难'),(2,'middle','中'),(3,'low','易');
/*!40000 ALTER TABLE `question_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_option`
--

DROP TABLE IF EXISTS `question_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question_option` (
  `question_option_id` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '题目选项表的主键',
  `question_option_content` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '选项的内容',
  `question_option_description` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '选项的额外描述，可以用于题目答案解析',
  `question_option_score` int DEFAULT '0',
  PRIMARY KEY (`question_option_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='题目的选项';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_option`
--

LOCK TABLES `question_option` WRITE;
/*!40000 ALTER TABLE `question_option` DISABLE KEYS */;
INSERT INTO `question_option` VALUES ('018d22b3d0cb4e67a9df8d23bdcb6ba9','所有特性分支都能在3天内合并到主干',NULL,3),('05fa17a379764e4e80bf4d600c3da752','	经常	',NULL,2),('076d9f3e3e1f42e4b9c93128348ccceb','11～15人',NULL,2),('098fde31a03f407fa94bcf0e86f8df56','	要为特定的项目长期维护不同的分支，活跃的主干及发布分支（2周内有提交记录）大于5个	',NULL,1),('0c1ee11629f6440886c51faf81c14daf','	无	',NULL,0),('0c7265efc30644479492b9afa4c97007','	一直如此	',NULL,3),('12e7a3cf71964e09acdfa504af761b1a','	有时	',NULL,1),('17f395ffe93b45eaaaf7d1580a2b0ee6','	有时	',NULL,1),('1af594bc97554d44b868dd388ade142c','	测试工作在编码完成后开始，如：编码开发完成后才开始设计测试用例，开展测试工作	',NULL,0),('1e9003997cd74939b2b54522efb2ff5d','	很少（贡献不足每月一次）	',NULL,0),('1fafd621f97c4a15ae2196cc900ed5ec','	具备集成的开发、测试和生产环境，且能及时将集成代码部署到各环境	',NULL,1),('205af17896824711a8ad4ac70ff34025','	有统一的制品构建产物和制品管理系统，具备清晰的存储结构	',NULL,1),('216f96dea9504f08bdafb361e04edde4','	部分时间在做架构、设计和实现相关工作	',NULL,2),('227d18f26e5147d08153b666b6061036','	具备完善的环境，并能够通过自动化的方式快速重建环境	',NULL,3),('255d517a6e464c73a6e684e076346d0','	所有特性分支都能在3天内合并到主干	',NULL,3),('255d517a6e464c73a6e684e076346d0e','测试工作提前至集成阶段，如：代码集成部署后即可开始实施用例测试',NULL,1),('25b1c4a4da904c9abc8dcfcbe27b0388','	无	',NULL,0),('26fd2597054c45328fb4f77f1697dc30','	从生产导出部分数据作为基准，并进行脱敏处理	',NULL,2),('28e933e61d3b4a9183f11b16cafbe9b6','	具有多层次、不同视角的架构详细说明和交互。如：系统上下文视角、容器视角、组件视角、代码视角来展现架构	',NULL,2),('29277d4c4fd34981b68c8fcec493e44b','一直如此',NULL,3),('29794c3b08854507ba9036d78639c246','	具备开发环境、测试环境、预生产环境、生产环境，且各个环境的职能清晰	',NULL,2),('2ef8dfe1a19c4bc3846536f5b3c50322','	无	',NULL,0),('2f845e0c2d7a4c018fb2aff73ccfe27b','测试工作和编码工作同步进行，如：通过TDD方式进行开发',NULL,3),('3124a840c5d442dbbebe85f94c966242','	每周两到四次	',NULL,2),('378939f1684946749c21eb3ab0e70f16','	经常（贡献不足每天一次）	',NULL,2),('3a7684eb0e3d4bf78b87f2c4cf371666','有时',NULL,0),('3e444a901c0a403d9650747e573814d6','	还有手动编译交付件的情况	',NULL,0),('3e7137452fe845b0980f56697447d8f6','	功能/接口的冒烟测试实现了自动化，但还不能做到在每条流水线上持续执行	',NULL,1),('3f0bc4a3cc86405badc63edad88643a7','	功能/接口的冒烟测试实现了自动化，可以在每条流水线上持续执行	',NULL,2),('41a4c6bfaf2249e4950388850576a319','	存在特性分支超过一个迭代不能合并主干的情况	',NULL,0),('45d33a01ae934e76b88cc3a1c2e396e2','	具有统一的代码规范要求	',NULL,1),('45f7f6e27def4944ac141cc26eaebb74','	每个测试用例的数据都可以通过模拟或调要用应用程序API生成	',NULL,3),('4753a830a38d4b8d8b4d8506950bf3d1','	能够不断完善代码规范，并通过经验分享、集体学习等方式落实代码规范要求	',NULL,3),('483d490ecb63467bb68b2204eddeabdd','	所有流水线的平均失败恢复时间大于等于8小时	',NULL,0),('49db849af3554873a36786498b12fd08','	每天	',NULL,3),('4e0fb5bec14646cabdb9ff211a88f4b6','	有时	',NULL,1),('500cc849186d4da3b65eac739b72a949','	小于每周一次	',NULL,0),('50fcf1a6bb1d49de91194be946a3398c','有时',NULL,0),('523f2172ec964c73a02eb65963eb362f','有时',NULL,0),('5277b1327e5f496eb2ef74eb3cfad690','一直如此',NULL,0),('55017c9e2d814b21906ad48b3131b6ff','	存在特性分支超过3天不能合并主干的情况	',NULL,2),('5619c8b299d8472d8f3f2a5bb4b728f8','	有基本的架构设计规范	',NULL,1),('56763cf56c5742f0a95a3a5853dcb175','要为特定的平台或者OS版本维护不同的分支，或者维护主干之外的开发分支',NULL,0),('5c91884aae70446b93e5c8a9ef884ad6','	并行的最多只有线上（上一次发布的hotfix）和主干（开发中）的两个分支	',NULL,3),('5e5bc2dbb84743b1ba615ef22245f4f6','经常',NULL,0),('613080cfc0444d6eaf17d51ed9f164c1','	无	',NULL,0),('63e23813ca204e97ae3beade42299f30','经常',NULL,0),('69350aff99944f93be566eaac8298e11','	自动化扫描规约与编码规范保持一致，能够自动化报告编码问题	',NULL,2),('707f5f38348144eba6af2dc40100e18f','核心的功能/接口测试实现了自动化，能保证每天都全量执行一次	',NULL,3),('716f8c4995a54f8395d1a9842e36f8fe','一直如此',NULL,0),('73ef86651be949c7a9f748dce8d54d81','	\"仅仅关注最基本的检查\n等门禁触发后才修改\"	',NULL,0),('792fd77e6bcf4423bacd12c5294fdf7c','	所有流水线的平均失败恢复时间小于8小时	',NULL,1),('7ea5fe5dc5cc43aa959428670524b807','	所有构建和发布都使用流水线自动完成且流水线上验证合理完整	',NULL,2),('805e732a65ba4bedaf31925ec5a4e928','4～10人',NULL,3),('86ffc8a5a8534cb1846695853348213d','有时',NULL,0),('87011881e47b496c9c7bde573da38dea','	无	',NULL,0),('88b6e22038884799b7f58f997f8911fb','	\"关注代码坏味道\n等门禁触发后才修改\"	',NULL,2),('8c3d3721c67a455fbe6bd4485bc6ef58','	很少	',NULL,0),('8de00c5e87eb45a2b48fa7eb61507bd4','	所有的依赖项都纳入到制品管理中，具有统一的可信的源	',NULL,2),('8f0ac1927722494483fdc77e39ea163f','	一直如此	',NULL,3),('907d68280d954c55b024b27f94ca26ca','	偶尔（贡献不足每周一次）	',NULL,1),('90f528cff8a2427d907032ca0eb6d8e3','	每周一次	',NULL,1),('9293da27354d44be989da92e0ddcce16','	很少	',NULL,0),('98ef90d88b7b4e9285c26fdfa9762f54','	绝大部分时间都在做架构、设计和实现相关工作	',NULL,3),('9d24347e52374a26938c749f5abbe021','经常',NULL,2),('a5150303ade24e36b5f740c7477258a7','	定期识别整理技术债	',NULL,1),('a95882064dce45dfab534d0bfc4d9011','	无	',NULL,0),('ab1990055ddf4980a9288c350096da6d','很少',NULL,0),('afab532ac02b41c28e1e7978c820ff15','	所有流水线的平均失败恢复时间小于4小时	',NULL,2),('b540053569434733897d30d353cc6b25','	有时	',NULL,1),('b7bfc6ef6bcb4519b1b01cd7a1a09172','	团队没有统一的分支策略，分支随意拉取，没有约束	',NULL,0),('ba7f599562884999ab279c79820a6ae7','	\"仅仅关注最基本的检查\n在提交前就尽可能杜绝\"	',NULL,1),('bc9a0957c0ae451c984dee4e1ea4b74a','	\"关注代码坏味道\n在提交前就尽可能杜绝\"	',NULL,3),('bcba47f4c69d434eabc1e2b3521ac07d','	频繁（贡献超过每天一次）	',NULL,3),('be3d04b7a4484e3a96fce8718c623bad','	经常	',NULL,2),('c394ac1d82db4a74b6b16a6fc0f04435','	能够依据规范严格执行并持续进行改进	',NULL,3),('c520e4299e534869b6a8de1a76a3c052','很少',NULL,0),('c9318a860d83403ea2457e7749247e12','	具有概要的架构说明，能够展示不同组件之间的交互、部署关系等	',NULL,1),('c987c8afa5a14baab968d3ae6b63e390','	对制品有明确的分级管理，实施明确的晋级策略	',NULL,3),('ca0bacf113044a87bf0f6027112967c2','很少',NULL,0),('ccd8bbb3d10a4fc5b686ec16e8d84ca7','	测试工作提前到编码阶段，如：某一类和方法编码后即对代码进行单元测试	',NULL,2),('cf992be5d6a34a17801d8b69fe93da97','	很少	',NULL,0),('d01d4873706d43de8578f40e7f5a2ca4','小于4人或大于20人',NULL,0),('d1390bc382c045ab9662bb50d6f29f37','	所有构建都使用流水线自动完成，但流水线上验证不合理或者不完整	',NULL,1),('d5232efb29da4790bc0189267557bad2','并行的最多只有线上（上一次发布的hotfix）和主干（开发中）的两个分支',NULL,0),('d54102b4c22c4eff9ecd9a532478777d','	所有流水线的平均失败恢复时间小于1小时	',NULL,3),('d6f8a0c07bce45c4b002e08f678107ff','	很少时间在做架构、设计和实现相关工作	',NULL,1),('d780694c60c341a495bc9ed686644464','经常',NULL,0),('d7a116d9d8e443a297953beb33497745','	团队成员能方便的查看架构视图，能够随时面对架构视图进行设计上的讨论，能够及时对架构视图进行改进并与代码保持一致	',NULL,3),('d7b2476e4ddc49729391d21c6e31fc2e','	团队中没有架构师或技术负责人	',NULL,0),('d7e17c36292e464486cb90e01ca23d4d','	每天一次	',NULL,3),('d89e5b9baf534245b227e72fa7fb70ee','有时',NULL,1),('db89b05b80d543eb8fb0870bb5650948','很少',NULL,0),('dbba3b5c93b74f9986e5278e3f53910c','很少',NULL,0),('dcacca977835474da4ce228677f2a604','	有基准的测试数据	',NULL,1),('dde527246b084992a8ea15626e1b8e4d','	功能/接口测试几乎全部靠手动执行	',NULL,0),('dde75d5c3d8741aabe971235e044f81e','	一直如此	',NULL,3),('de106849ca2641b3927542ac81392d6b','	遵循TBD分支策略所需的流水线配置，并对流水线的构建进行了优化调整	',NULL,3),('e05134eff2124345a58004ca501988c3','一直如此',NULL,0),('e2771033cab54d45ba66c14aa6f7649c','	经常	',NULL,2),('e28ced22550e484eac308c3253057b55','要为特定的整机项目维护不同的分支',NULL,0),('e3a3e23924614d038bdc9a2563b06e8a','	要为特定的项目长期维护不同的分支，活跃的主干及发布分支（2周内有提交记录）大于3个	',NULL,2),('e3bd9c02871e469b938af2b41268ef2a','	没有集成的开发环境，每个开发人员在自己的开发环境中进行联调和测试	',NULL,0),('e3c23e899eb94c00979d04a77e4a4100','	建议技术债管理机制，技术债持续降低，并保持较低的水平	',NULL,3),('e5cab4ca0da74291bc92eaae041417d5','	经常	',NULL,2),('e78a8c28d2d04d0fbea1c22ac44e22e8','16人～20人',NULL,1),('e8cb5ddd2fa24b44b3ad093b8b6d5ed6','	有计划的偿还技术债	',NULL,2),('eb278bf1403a4d96b6c958a2d8dbf154','经常',NULL,0),('ed3b5b8a06544ec5b7d81b62879719e1','一直如此',NULL,0),('efdbc47f07ef4608924c78d95320eef2','	很少	',NULL,0),('f3a9d4e421bb4d5a8028fea4d8b68e3a','	存在特性分支超过一周迭代不能合并主干的情况	',NULL,1),('ffc889228a1c4855bed40a5e7c3d9818','	具备详细的架构设计规范	',NULL,2);
/*!40000 ALTER TABLE `question_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_type`
--

DROP TABLE IF EXISTS `question_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question_type` (
  `question_type_id` int NOT NULL AUTO_INCREMENT COMMENT '题目类型表的主键',
  `question_type_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '题目类型名称',
  `question_type_description` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '题目类型的描述',
  PRIMARY KEY (`question_type_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='问题类型';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_type`
--

LOCK TABLES `question_type` WRITE;
/*!40000 ALTER TABLE `question_type` DISABLE KEYS */;
INSERT INTO `question_type` VALUES (1,'产品','产品'),(2,'管理','管理'),(3,'技术','技术');
/*!40000 ALTER TABLE `question_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `role_id` int NOT NULL AUTO_INCREMENT COMMENT '角色表主键id',
  `role_name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '角色名称',
  `role_description` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '角色的描述',
  `role_detail` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '角色的详细功能阐述',
  `role_page_ids` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '当前角色所能访问的页面的id集合',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='用户角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin','管理员','拥有教师和学生的所有权限','1-2-3-4-5-6'),(2,'teacher','教师','出题、组试卷、管理学生和试卷','1-2-3-4-5-6'),(3,'student','学生','参与考试，查看分数','1-2-3-6');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `user_id` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '用户id,主键，字符串型',
  `user_username` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '用户名',
  `user_nickname` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '用户昵称',
  `user_password` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT '用户秘密',
  `user_role_id` int NOT NULL COMMENT '当前用户的角色的id',
  `user_avatar` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '用户的头像地址',
  `user_description` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '用户的自我描述',
  `user_email` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '用户邮箱',
  `user_phone` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL COMMENT '用户手机号',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE KEY `user_username` (`user_username`) USING BTREE,
  UNIQUE KEY `user_email` (`user_email`) USING BTREE,
  UNIQUE KEY `user_phone` (`user_phone`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin ROW_FORMAT=DYNAMIC COMMENT='用户信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('68042014e23c4ebea7234cb9c77cee5c','teacher','覃老板','YWRtaW4xMjM=',2,'https://i.loli.net/2019/11/02/OUfHKhMSwRv1ntX.jpg','快乐就好','1648266192@qq.com','15261897332','2019-05-06 10:03:27','2020-08-24 09:40:46'),('79392778a90d4639a297dbd0bae0f779','student','小菜','YWRtaW4xMjM=',3,'https://i.loli.net/2019/11/02/rCHKVJd4jTovzW9.jpg','好好学习，天天向上','liangshanguang@huawei.com','17712345678','2019-05-06 10:07:14','2020-08-24 09:40:46'),('a1b661031adf4a8f969f1869d479fe74','admin','彩虹风暴','YWRtaW4xMjM=',1,'https://i.loli.net/2019/11/02/DvPiSRJrzoH1tkZ.gif','绳锯木断，水滴石穿','liangshanguang2@gmail.com','17601324488','2019-05-06 09:57:44','2020-08-24 09:40:46');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-03 23:13:24
